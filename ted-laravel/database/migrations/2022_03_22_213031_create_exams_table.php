<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exams', function (Blueprint $table) {
            $table->id();
            $table->integer('teacher_lessons_id')->nullable();
            $table->integer('student_id')->nullable();

            $table->integer('exam_score_1')->nullable();
            $table->integer('exam_score_2')->nullable();
            $table->integer('exam_score_3')->nullable();
            $table->integer('performance_score_1')->nullable();
            $table->integer('performance_score_2')->nullable();
            $table->integer('project_score')->nullable();
            $table->integer('summary_score')->nullable();

            $table->integer('status')->default("1");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exams');
    }
}
