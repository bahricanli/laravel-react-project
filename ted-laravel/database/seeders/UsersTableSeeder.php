<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Bahri Meriç Canlı',
                'email' => 'bahri@bahri.info',
                'email_verified_at' => NULL,
                'password' => '$2y$10$o1kK25YU5KDYNdrDtihJ0ucUELdEozqUQLIWpRbbfGQKNMSW2uRfK',
                'remember_token' => NULL,
                'status' => 1,
                'role' => 'admin',
                'created_at' => '2022-03-20 14:07:50',
                'updated_at' => '2022-03-21 15:05:08',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Seçil Demirkol Canlı',
                'email' => 'secildemirkol90@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$gZ.YvhmOFVU.5ZrdGNRfLeTvWWshJoL8PPC00fDCHSWTD1UTyMWQe',
                'remember_token' => NULL,
                'status' => 0,
                'role' => 'teacher',
                'created_at' => '2022-03-21 15:11:41',
                'updated_at' => '2022-03-23 07:12:03',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Bahri Canlı',
                'email' => 'bmericc@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$PGkEGXmzU8ltufJxrY4L7e1IykCi0khBy7JR0106wzVCJV8ZtDQ16',
                'remember_token' => NULL,
                'status' => 0,
                'role' => 'teacher',
                'created_at' => '2022-03-21 15:24:40',
                'updated_at' => '2022-03-23 07:12:06',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Serkan ÇIRACIOĞLU',
                'email' => 'serkan@tedankara.k12.tr',
                'email_verified_at' => NULL,
                'password' => '$2y$10$WHB0XLSAxlJgdill2H3uMeveXNugtfcjeI69yXsTLIHvuXdJIgvfG',
                'remember_token' => NULL,
                'status' => 1,
                'role' => 'admin',
                'created_at' => '2022-03-23 17:05:43',
                'updated_at' => '2022-03-23 17:05:43',
            ),
        ));
        
        
    }
}