<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LessonsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('lessons')->delete();
        
        \DB::table('lessons')->insert(array (
            0 => 
            array (
                'id' => 1,
                'lesson_name' => 'Matematik',
                'status' => 1,
                'created_at' => NULL,
                'updated_at' => '2022-03-20 20:59:54',
            ),
            1 => 
            array (
                'id' => 2,
                'lesson_name' => 'Kimya',
                'status' => 1,
                'created_at' => '2022-03-20 20:59:45',
                'updated_at' => '2022-03-20 20:59:45',
            ),
            2 => 
            array (
                'id' => 3,
            'lesson_name' => '11. Sınıf Matematik (Seçmeli)',
                'status' => 1,
                'created_at' => '2022-03-20 21:22:27',
                'updated_at' => '2022-03-21 15:59:05',
            ),
            3 => 
            array (
                'id' => 4,
                'lesson_name' => '11. Sınıf Matematik',
                'status' => 1,
                'created_at' => '2022-03-21 15:58:55',
                'updated_at' => '2022-03-21 15:58:55',
            ),
            4 => 
            array (
                'id' => 5,
                'lesson_name' => 'Fizik',
                'status' => 1,
                'created_at' => '2022-03-21 15:59:19',
                'updated_at' => '2022-03-21 15:59:19',
            ),
            5 => 
            array (
                'id' => 6,
                'lesson_name' => 'Türkçe',
                'status' => 1,
                'created_at' => '2022-03-21 15:59:27',
                'updated_at' => '2022-03-21 15:59:27',
            ),
        ));
        
        
    }
}