<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TeachersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('teachers')->delete();
        
        \DB::table('teachers')->insert(array (
            0 => 
            array (
                'id' => 1,
                'teacher_name' => 'Ayşegül',
                'teacher_surname' => 'Batı',
                'status' => 0,
                'created_at' => NULL,
                'updated_at' => '2022-03-20 21:09:31',
            ),
            1 => 
            array (
                'id' => 2,
                'teacher_name' => 'Volkan',
                'teacher_surname' => 'Konak',
                'status' => 1,
                'created_at' => '2022-03-20 20:35:20',
                'updated_at' => '2022-03-20 20:35:20',
            ),
            2 => 
            array (
                'id' => 3,
                'teacher_name' => 'Emre',
                'teacher_surname' => 'Aydın',
                'status' => 0,
                'created_at' => '2022-03-20 21:09:51',
                'updated_at' => '2022-03-20 21:09:54',
            ),
            3 => 
            array (
                'id' => 4,
                'teacher_name' => 'Kazım',
                'teacher_surname' => 'Koyuncu',
                'status' => 1,
                'created_at' => '2022-03-20 21:11:50',
                'updated_at' => '2022-03-20 21:11:50',
            ),
            4 => 
            array (
                'id' => 5,
                'teacher_name' => 'Ümit',
                'teacher_surname' => 'Besen',
                'status' => 1,
                'created_at' => '2022-03-20 21:11:59',
                'updated_at' => '2022-03-20 21:11:59',
            ),
            5 => 
            array (
                'id' => 6,
                'teacher_name' => 'Mahmut',
                'teacher_surname' => 'Tuncer',
                'status' => 1,
                'created_at' => '2022-03-20 21:12:07',
                'updated_at' => '2022-03-20 21:12:07',
            ),
            6 => 
            array (
                'id' => 7,
                'teacher_name' => 'Barış',
                'teacher_surname' => 'Manço',
                'status' => 1,
                'created_at' => '2022-03-20 21:19:43',
                'updated_at' => '2022-03-20 21:19:43',
            ),
        ));
        
        
    }
}