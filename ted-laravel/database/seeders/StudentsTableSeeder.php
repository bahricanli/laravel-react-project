<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('students')->delete();
        
        \DB::table('students')->insert(array (
            0 => 
            array (
                'id' => 1,
                'identity_number' => 13001,
                'student_name' => 'Ora',
                'student_surname' => 'Klein',
                'class_id' => 1,
                'status' => 1,
                'created_at' => '2022-03-20 20:35:05',
                'updated_at' => '2021-12-25 20:35:05',
            ),
            1 => 
            array (
                'id' => 2,
                'identity_number' => 13002,
                'student_name' => 'Beulah',
                'student_surname' => 'McMillan',
                'class_id' => 1,
                'status' => 1,
                'created_at' => '2022-03-21 20:35:05',
                'updated_at' => '2021-12-26 20:35:05',
            ),
            2 => 
            array (
                'id' => 3,
                'identity_number' => 13003,
                'student_name' => 'Florence',
                'student_surname' => 'Stark',
                'class_id' => 1,
                'status' => 1,
                'created_at' => '2022-03-22 20:35:05',
                'updated_at' => '2021-12-27 20:35:05',
            ),
            3 => 
            array (
                'id' => 4,
                'identity_number' => 13004,
                'student_name' => 'Noah',
                'student_surname' => 'Gilbertson',
                'class_id' => 1,
                'status' => 1,
                'created_at' => '2022-03-23 20:35:05',
                'updated_at' => '2021-12-28 20:35:05',
            ),
            4 => 
            array (
                'id' => 5,
                'identity_number' => 13005,
                'student_name' => 'Erna',
                'student_surname' => 'Parker',
                'class_id' => 1,
                'status' => 1,
                'created_at' => '2022-03-24 20:35:05',
                'updated_at' => '2021-12-29 20:35:05',
            ),
            5 => 
            array (
                'id' => 6,
                'identity_number' => 13006,
                'student_name' => 'Sherry',
                'student_surname' => 'Santana',
                'class_id' => 1,
                'status' => 1,
                'created_at' => '2022-03-25 20:35:05',
                'updated_at' => '2021-12-30 20:35:05',
            ),
            6 => 
            array (
                'id' => 7,
                'identity_number' => 13007,
                'student_name' => 'Ronald',
                'student_surname' => 'Lees',
                'class_id' => 1,
                'status' => 1,
                'created_at' => '2022-03-26 20:35:05',
                'updated_at' => '2021-12-31 20:35:05',
            ),
            7 => 
            array (
                'id' => 8,
                'identity_number' => 13008,
                'student_name' => 'Latasha',
                'student_surname' => 'Pratt',
                'class_id' => 1,
                'status' => 1,
                'created_at' => '2022-03-27 20:35:05',
                'updated_at' => '2022-01-01 20:35:05',
            ),
            8 => 
            array (
                'id' => 9,
                'identity_number' => 13009,
                'student_name' => 'Misty',
                'student_surname' => 'Thomas',
                'class_id' => 1,
                'status' => 1,
                'created_at' => '2022-03-28 20:35:05',
                'updated_at' => '2022-01-02 20:35:05',
            ),
            9 => 
            array (
                'id' => 10,
                'identity_number' => 13010,
                'student_name' => 'Petra',
                'student_surname' => 'Barlow',
                'class_id' => 1,
                'status' => 1,
                'created_at' => '2022-03-29 20:35:05',
                'updated_at' => '2022-01-03 20:35:05',
            ),
            10 => 
            array (
                'id' => 11,
                'identity_number' => 13011,
                'student_name' => 'Bonnie',
                'student_surname' => 'Hampton',
                'class_id' => 1,
                'status' => 1,
                'created_at' => '2022-03-30 20:35:05',
                'updated_at' => '2022-01-04 20:35:05',
            ),
            11 => 
            array (
                'id' => 12,
                'identity_number' => 13012,
                'student_name' => 'Dion',
                'student_surname' => 'Matheson',
                'class_id' => 1,
                'status' => 1,
                'created_at' => '2022-03-31 20:35:05',
                'updated_at' => '2022-01-05 20:35:05',
            ),
            12 => 
            array (
                'id' => 13,
                'identity_number' => 13013,
                'student_name' => 'Cesar',
                'student_surname' => 'McCray',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-01 20:35:05',
                'updated_at' => '2022-01-06 20:35:05',
            ),
            13 => 
            array (
                'id' => 14,
                'identity_number' => 13014,
                'student_name' => 'Rickey',
                'student_surname' => 'Cottle',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-02 20:35:05',
                'updated_at' => '2022-01-07 20:35:05',
            ),
            14 => 
            array (
                'id' => 15,
                'identity_number' => 13015,
                'student_name' => 'Fredrick',
                'student_surname' => 'Markley',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-03 20:35:05',
                'updated_at' => '2022-01-08 20:35:05',
            ),
            15 => 
            array (
                'id' => 16,
                'identity_number' => 13016,
                'student_name' => 'Joshua',
                'student_surname' => 'Bartels',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-04 20:35:05',
                'updated_at' => '2022-01-09 20:35:05',
            ),
            16 => 
            array (
                'id' => 17,
                'identity_number' => 13017,
                'student_name' => 'Maribel',
                'student_surname' => 'Parsons',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-05 20:35:05',
                'updated_at' => '2022-01-10 20:35:05',
            ),
            17 => 
            array (
                'id' => 18,
                'identity_number' => 13018,
                'student_name' => 'Erma',
                'student_surname' => 'Ballard',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-06 20:35:05',
                'updated_at' => '2022-01-11 20:35:05',
            ),
            18 => 
            array (
                'id' => 19,
                'identity_number' => 13019,
                'student_name' => 'Wilfred',
                'student_surname' => 'Bevins',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-07 20:35:05',
                'updated_at' => '2022-01-12 20:35:05',
            ),
            19 => 
            array (
                'id' => 20,
                'identity_number' => 13020,
                'student_name' => 'Rogelio',
                'student_surname' => 'Cazares',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-08 20:35:05',
                'updated_at' => '2022-01-13 20:35:05',
            ),
            20 => 
            array (
                'id' => 21,
                'identity_number' => 13021,
                'student_name' => 'Dixie',
                'student_surname' => 'Morrison',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-09 20:35:05',
                'updated_at' => '2022-01-14 20:35:05',
            ),
            21 => 
            array (
                'id' => 22,
                'identity_number' => 13022,
                'student_name' => 'Williams',
                'student_surname' => 'Bisson',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-10 20:35:05',
                'updated_at' => '2022-01-15 20:35:05',
            ),
            22 => 
            array (
                'id' => 23,
                'identity_number' => 13023,
                'student_name' => 'Christie',
                'student_surname' => 'Clayton',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-11 20:35:05',
                'updated_at' => '2022-01-16 20:35:05',
            ),
            23 => 
            array (
                'id' => 24,
                'identity_number' => 13024,
                'student_name' => 'Angelina',
                'student_surname' => 'Craig',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-12 20:35:05',
                'updated_at' => '2022-01-17 20:35:05',
            ),
            24 => 
            array (
                'id' => 25,
                'identity_number' => 13025,
                'student_name' => 'Earnest',
                'student_surname' => 'Rome',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-13 20:35:05',
                'updated_at' => '2022-01-18 20:35:05',
            ),
            25 => 
            array (
                'id' => 26,
                'identity_number' => 13026,
                'student_name' => 'Madeline',
                'student_surname' => 'Sargent',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-14 20:35:05',
                'updated_at' => '2022-01-19 20:35:05',
            ),
            26 => 
            array (
                'id' => 27,
                'identity_number' => 13027,
                'student_name' => 'Gene',
                'student_surname' => 'Angulo',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-15 20:35:05',
                'updated_at' => '2022-01-20 20:35:05',
            ),
            27 => 
            array (
                'id' => 28,
                'identity_number' => 13028,
                'student_name' => 'Daphne',
                'student_surname' => 'Bentley',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-16 20:35:05',
                'updated_at' => '2022-01-21 20:35:05',
            ),
            28 => 
            array (
                'id' => 29,
                'identity_number' => 13029,
                'student_name' => 'Dora',
                'student_surname' => 'Montgomery',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-17 20:35:05',
                'updated_at' => '2022-01-22 20:35:05',
            ),
            29 => 
            array (
                'id' => 30,
                'identity_number' => 13030,
                'student_name' => 'Bertha',
                'student_surname' => 'Nolan',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-18 20:35:05',
                'updated_at' => '2022-01-23 20:35:05',
            ),
            30 => 
            array (
                'id' => 31,
                'identity_number' => 13031,
                'student_name' => 'Stacey',
                'student_surname' => 'Foltz',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-19 20:35:05',
                'updated_at' => '2022-01-24 20:35:05',
            ),
            31 => 
            array (
                'id' => 32,
                'identity_number' => 13032,
                'student_name' => 'Wilma',
                'student_surname' => 'Aguirre',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-20 20:35:05',
                'updated_at' => '2022-01-25 20:35:05',
            ),
            32 => 
            array (
                'id' => 33,
                'identity_number' => 13033,
                'student_name' => 'Forrest',
                'student_surname' => 'Downes',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-21 20:35:05',
                'updated_at' => '2022-01-26 20:35:05',
            ),
            33 => 
            array (
                'id' => 34,
                'identity_number' => 13034,
                'student_name' => 'Noah',
                'student_surname' => 'Roger',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-22 20:35:05',
                'updated_at' => '2022-01-27 20:35:05',
            ),
            34 => 
            array (
                'id' => 35,
                'identity_number' => 13035,
                'student_name' => 'Tracy',
                'student_surname' => 'Baxley',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-23 20:35:05',
                'updated_at' => '2022-01-28 20:35:05',
            ),
            35 => 
            array (
                'id' => 36,
                'identity_number' => 13036,
                'student_name' => 'Federico',
                'student_surname' => 'Fiore',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-24 20:35:05',
                'updated_at' => '2022-01-29 20:35:05',
            ),
            36 => 
            array (
                'id' => 37,
                'identity_number' => 13037,
                'student_name' => 'Nancy',
                'student_surname' => 'Mendoza',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-25 20:35:05',
                'updated_at' => '2022-01-30 20:35:05',
            ),
            37 => 
            array (
                'id' => 38,
                'identity_number' => 13038,
                'student_name' => 'Luther',
                'student_surname' => 'Briley',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-26 20:35:05',
                'updated_at' => '2022-01-31 20:35:05',
            ),
            38 => 
            array (
                'id' => 39,
                'identity_number' => 13039,
                'student_name' => 'Norbert',
                'student_surname' => 'Lunn',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-27 20:35:05',
                'updated_at' => '2022-02-01 20:35:05',
            ),
            39 => 
            array (
                'id' => 40,
                'identity_number' => 13040,
                'student_name' => 'Fidel',
                'student_surname' => 'Obryan',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-28 20:35:05',
                'updated_at' => '2022-02-02 20:35:05',
            ),
            40 => 
            array (
                'id' => 41,
                'identity_number' => 13041,
                'student_name' => 'Byron',
                'student_surname' => 'Montenegro',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-29 20:35:05',
                'updated_at' => '2022-02-03 20:35:05',
            ),
            41 => 
            array (
                'id' => 42,
                'identity_number' => 13042,
                'student_name' => 'Blaine',
                'student_surname' => 'Dulaney',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-04-30 20:35:05',
                'updated_at' => '2022-02-04 20:35:05',
            ),
            42 => 
            array (
                'id' => 43,
                'identity_number' => 13043,
                'student_name' => 'Dennis',
                'student_surname' => 'Word',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-01 20:35:05',
                'updated_at' => '2022-02-05 20:35:05',
            ),
            43 => 
            array (
                'id' => 44,
                'identity_number' => 13044,
                'student_name' => 'Coy',
                'student_surname' => 'Whiteman',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-02 20:35:05',
                'updated_at' => '2022-02-06 20:35:05',
            ),
            44 => 
            array (
                'id' => 45,
                'identity_number' => 13045,
                'student_name' => 'Rosie',
                'student_surname' => 'Ingram',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-03 20:35:05',
                'updated_at' => '2022-02-07 20:35:05',
            ),
            45 => 
            array (
                'id' => 46,
                'identity_number' => 13046,
                'student_name' => 'Anita',
                'student_surname' => 'Miranda',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-04 20:35:05',
                'updated_at' => '2022-02-08 20:35:05',
            ),
            46 => 
            array (
                'id' => 47,
                'identity_number' => 13047,
                'student_name' => 'Jerri',
                'student_surname' => 'Noel',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-05 20:35:05',
                'updated_at' => '2022-02-09 20:35:05',
            ),
            47 => 
            array (
                'id' => 48,
                'identity_number' => 13048,
                'student_name' => 'Dona',
                'student_surname' => 'Small',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-06 20:35:05',
                'updated_at' => '2022-02-10 20:35:05',
            ),
            48 => 
            array (
                'id' => 49,
                'identity_number' => 13049,
                'student_name' => 'Erik',
                'student_surname' => 'Theriault',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-07 20:35:05',
                'updated_at' => '2022-02-11 20:35:05',
            ),
            49 => 
            array (
                'id' => 50,
                'identity_number' => 13050,
                'student_name' => 'Earnestine',
                'student_surname' => 'Mosley',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-08 20:35:05',
                'updated_at' => '2022-02-12 20:35:05',
            ),
            50 => 
            array (
                'id' => 51,
                'identity_number' => 13051,
                'student_name' => 'Jimmie',
                'student_surname' => 'Petty',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-09 20:35:05',
                'updated_at' => '2022-02-13 20:35:05',
            ),
            51 => 
            array (
                'id' => 52,
                'identity_number' => 13052,
                'student_name' => 'Burton',
                'student_surname' => 'Saul',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-10 20:35:05',
                'updated_at' => '2022-02-14 20:35:05',
            ),
            52 => 
            array (
                'id' => 53,
                'identity_number' => 13053,
                'student_name' => 'Juanita',
                'student_surname' => 'Hardy',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-11 20:35:05',
                'updated_at' => '2022-02-15 20:35:05',
            ),
            53 => 
            array (
                'id' => 54,
                'identity_number' => 13054,
                'student_name' => 'Benny',
                'student_surname' => 'Vanover',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-12 20:35:05',
                'updated_at' => '2022-02-16 20:35:05',
            ),
            54 => 
            array (
                'id' => 55,
                'identity_number' => 13055,
                'student_name' => 'Harrison',
                'student_surname' => 'Badillo',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-13 20:35:05',
                'updated_at' => '2022-02-17 20:35:05',
            ),
            55 => 
            array (
                'id' => 56,
                'identity_number' => 13056,
                'student_name' => 'Jorge',
                'student_surname' => 'Bevins',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-14 20:35:05',
                'updated_at' => '2022-02-18 20:35:05',
            ),
            56 => 
            array (
                'id' => 57,
                'identity_number' => 13057,
                'student_name' => 'Briana',
                'student_surname' => 'Tyson',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-15 20:35:05',
                'updated_at' => '2022-02-19 20:35:05',
            ),
            57 => 
            array (
                'id' => 58,
                'identity_number' => 13058,
                'student_name' => 'Shelby',
                'student_surname' => 'Abbott',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-16 20:35:05',
                'updated_at' => '2022-02-20 20:35:05',
            ),
            58 => 
            array (
                'id' => 59,
                'identity_number' => 13059,
                'student_name' => 'Carlos',
                'student_surname' => 'Lauer',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-17 20:35:05',
                'updated_at' => '2022-02-21 20:35:05',
            ),
            59 => 
            array (
                'id' => 60,
                'identity_number' => 13060,
                'student_name' => 'Kristi',
                'student_surname' => 'Kaufman',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-18 20:35:05',
                'updated_at' => '2022-02-22 20:35:05',
            ),
            60 => 
            array (
                'id' => 61,
                'identity_number' => 13061,
                'student_name' => 'Sophia',
                'student_surname' => 'Wilder',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-19 20:35:05',
                'updated_at' => '2022-02-23 20:35:05',
            ),
            61 => 
            array (
                'id' => 62,
                'identity_number' => 13062,
                'student_name' => 'Angelita',
                'student_surname' => 'Valentine',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-20 20:35:05',
                'updated_at' => '2022-02-24 20:35:05',
            ),
            62 => 
            array (
                'id' => 63,
                'identity_number' => 13063,
                'student_name' => 'Dexter',
                'student_surname' => 'Wyant',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-21 20:35:05',
                'updated_at' => '2022-02-25 20:35:05',
            ),
            63 => 
            array (
                'id' => 64,
                'identity_number' => 13064,
                'student_name' => 'Monique',
                'student_surname' => 'McMillan',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-22 20:35:05',
                'updated_at' => '2022-02-26 20:35:05',
            ),
            64 => 
            array (
                'id' => 65,
                'identity_number' => 13065,
                'student_name' => 'Miles',
                'student_surname' => 'Runyan',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-23 20:35:05',
                'updated_at' => '2022-02-27 20:35:05',
            ),
            65 => 
            array (
                'id' => 66,
                'identity_number' => 13066,
                'student_name' => 'Liza',
                'student_surname' => 'Grimes',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-24 20:35:05',
                'updated_at' => '2022-02-28 20:35:05',
            ),
            66 => 
            array (
                'id' => 67,
                'identity_number' => 13067,
                'student_name' => 'Mara',
                'student_surname' => 'Alford',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-25 20:35:05',
                'updated_at' => '2022-03-01 20:35:05',
            ),
            67 => 
            array (
                'id' => 68,
                'identity_number' => 13068,
                'student_name' => 'Alison',
                'student_surname' => 'Ochoa',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-26 20:35:05',
                'updated_at' => '2022-03-02 20:35:05',
            ),
            68 => 
            array (
                'id' => 69,
                'identity_number' => 13069,
                'student_name' => 'Leslie',
                'student_surname' => 'Navarro',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-27 20:35:05',
                'updated_at' => '2022-03-03 20:35:05',
            ),
            69 => 
            array (
                'id' => 70,
                'identity_number' => 13070,
                'student_name' => 'Jo',
                'student_surname' => 'Ochoa',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-28 20:35:05',
                'updated_at' => '2022-03-04 20:35:05',
            ),
            70 => 
            array (
                'id' => 71,
                'identity_number' => 13071,
                'student_name' => 'Jacklyn',
                'student_surname' => 'Steele',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-29 20:35:05',
                'updated_at' => '2022-03-05 20:35:05',
            ),
            71 => 
            array (
                'id' => 72,
                'identity_number' => 13072,
                'student_name' => 'Karyn',
                'student_surname' => 'Dale',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-30 20:35:05',
                'updated_at' => '2022-03-06 20:35:05',
            ),
            72 => 
            array (
                'id' => 73,
                'identity_number' => 13073,
                'student_name' => 'Ines',
                'student_surname' => 'Meyers',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-05-31 20:35:05',
                'updated_at' => '2022-03-07 20:35:05',
            ),
            73 => 
            array (
                'id' => 74,
                'identity_number' => 13074,
                'student_name' => 'Vincent',
                'student_surname' => 'Doucette',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-06-01 20:35:05',
                'updated_at' => '2022-03-08 20:35:05',
            ),
            74 => 
            array (
                'id' => 75,
                'identity_number' => 13075,
                'student_name' => 'Noemi',
                'student_surname' => 'Chase',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-06-02 20:35:05',
                'updated_at' => '2022-03-09 20:35:05',
            ),
            75 => 
            array (
                'id' => 76,
                'identity_number' => 13076,
                'student_name' => 'Daryl',
                'student_surname' => 'Flemming',
                'class_id' => 3,
                'status' => 1,
                'created_at' => '2022-06-03 20:35:05',
                'updated_at' => '2022-03-10 20:35:05',
            ),
            76 => 
            array (
                'id' => 77,
                'identity_number' => 13077,
                'student_name' => 'Winnie',
                'student_surname' => 'Carson',
                'class_id' => 3,
                'status' => 1,
                'created_at' => '2022-06-04 20:35:05',
                'updated_at' => '2022-03-11 20:35:05',
            ),
            77 => 
            array (
                'id' => 78,
                'identity_number' => 13078,
                'student_name' => 'Ilene',
                'student_surname' => 'Valentine',
                'class_id' => 3,
                'status' => 1,
                'created_at' => '2022-06-05 20:35:05',
                'updated_at' => '2022-03-12 20:35:05',
            ),
            78 => 
            array (
                'id' => 79,
                'identity_number' => 13079,
                'student_name' => 'Alton',
                'student_surname' => 'Wooley',
                'class_id' => 3,
                'status' => 1,
                'created_at' => '2022-06-06 20:35:05',
                'updated_at' => '2022-03-13 20:35:05',
            ),
            79 => 
            array (
                'id' => 80,
                'identity_number' => 13080,
                'student_name' => 'Lizzie',
                'student_surname' => 'Stokes',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-06-07 20:35:05',
                'updated_at' => '2022-03-14 20:35:05',
            ),
            80 => 
            array (
                'id' => 81,
                'identity_number' => 13081,
                'student_name' => 'Amos',
                'student_surname' => 'Crumpton',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-06-08 20:35:05',
                'updated_at' => '2022-03-15 20:35:05',
            ),
            81 => 
            array (
                'id' => 82,
                'identity_number' => 13082,
                'student_name' => 'Belinda',
                'student_surname' => 'Orr',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-06-09 20:35:05',
                'updated_at' => '2022-03-16 20:35:05',
            ),
            82 => 
            array (
                'id' => 83,
                'identity_number' => 13083,
                'student_name' => 'Desiree',
                'student_surname' => 'Hawkins',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-06-10 20:35:05',
                'updated_at' => '2022-03-17 20:35:05',
            ),
            83 => 
            array (
                'id' => 84,
                'identity_number' => 13084,
                'student_name' => 'Dane',
                'student_surname' => 'McCullough',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-06-11 20:35:05',
                'updated_at' => '2022-03-18 20:35:05',
            ),
            84 => 
            array (
                'id' => 85,
                'identity_number' => 13085,
                'student_name' => 'Genevieve',
                'student_surname' => 'Cole',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-06-12 20:35:05',
                'updated_at' => '2022-03-19 20:35:05',
            ),
            85 => 
            array (
                'id' => 86,
                'identity_number' => 13086,
                'student_name' => 'Ramiro',
                'student_surname' => 'Skeen',
                'class_id' => 2,
                'status' => 1,
                'created_at' => '2022-06-13 20:35:05',
                'updated_at' => '2022-03-20 20:35:05',
            ),
        ));
        
        
    }
}