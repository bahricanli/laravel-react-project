<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ClassesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('classes')->delete();
        
        \DB::table('classes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'class_name' => '11 D',
                'year' => '2021 - 2022',
                'status' => 1,
                'created_at' => NULL,
                'updated_at' => '2022-03-21 18:14:39',
            ),
            1 => 
            array (
                'id' => 2,
                'class_name' => '11 A',
                'year' => '2021 - 2022',
                'status' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'class_name' => '11 C',
                'year' => '2021 - 2022',
                'status' => 1,
                'created_at' => '2022-03-20 20:39:04',
                'updated_at' => '2022-03-20 20:39:14',
            ),
            3 => 
            array (
                'id' => 4,
                'class_name' => '11 B',
                'year' => '2021 - 2022',
                'status' => 1,
                'created_at' => '2022-03-20 21:19:56',
                'updated_at' => '2022-03-20 21:19:56',
            ),
        ));
        
        
    }
}