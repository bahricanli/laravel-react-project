<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TeacherLessonsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('teacher_lessons')->delete();
        
        \DB::table('teacher_lessons')->insert(array (
            0 => 
            array (
                'id' => 1,
                'teacher_id' => 2,
                'class_id' => 1,
                'lesson_id' => 3,
                'status' => 1,
                'created_at' => '2022-03-22 16:28:17',
                'updated_at' => '2022-03-22 16:28:17',
            ),
            1 => 
            array (
                'id' => 2,
                'teacher_id' => 2,
                'class_id' => 2,
                'lesson_id' => 3,
                'status' => 1,
                'created_at' => '2022-03-22 16:29:25',
                'updated_at' => '2022-03-22 16:29:25',
            ),
            2 => 
            array (
                'id' => 3,
                'teacher_id' => 2,
                'class_id' => 4,
                'lesson_id' => 4,
                'status' => 1,
                'created_at' => '2022-03-22 16:31:19',
                'updated_at' => '2022-03-22 16:31:19',
            ),
            3 => 
            array (
                'id' => 4,
                'teacher_id' => 4,
                'class_id' => 4,
                'lesson_id' => 1,
                'status' => 1,
                'created_at' => '2022-03-22 18:56:07',
                'updated_at' => '2022-03-22 18:56:07',
            ),
            4 => 
            array (
                'id' => 5,
                'teacher_id' => 5,
                'class_id' => 3,
                'lesson_id' => 2,
                'status' => 1,
                'created_at' => '2022-03-22 18:56:23',
                'updated_at' => '2022-03-22 18:56:23',
            ),
        ));
        
        
    }
}