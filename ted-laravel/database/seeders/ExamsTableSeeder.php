<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ExamsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('exams')->delete();
        
        \DB::table('exams')->insert(array (
            0 => 
            array (
                'id' => 1,
                'teacher_lessons_id' => 1,
                'student_id' => 1,
                'exam_score_1' => NULL,
                'exam_score_2' => 75,
                'exam_score_3' => NULL,
                'performance_score_1' => NULL,
                'performance_score_2' => NULL,
                'project_score' => NULL,
                'summary_score' => 75,
                'status' => 1,
                'created_at' => '2022-03-23 14:41:12',
                'updated_at' => '2022-03-23 16:49:45',
            ),
            1 => 
            array (
                'id' => 2,
                'teacher_lessons_id' => 1,
                'student_id' => 3,
                'exam_score_1' => 100,
                'exam_score_2' => NULL,
                'exam_score_3' => NULL,
                'performance_score_1' => 75,
                'performance_score_2' => NULL,
                'project_score' => NULL,
                'summary_score' => 88,
                'status' => 1,
                'created_at' => '2022-03-23 16:50:30',
                'updated_at' => '2022-03-23 16:50:30',
            ),
            2 => 
            array (
                'id' => 3,
                'teacher_lessons_id' => 1,
                'student_id' => 5,
                'exam_score_1' => 100,
                'exam_score_2' => 55,
                'exam_score_3' => NULL,
                'performance_score_1' => NULL,
                'performance_score_2' => NULL,
                'project_score' => NULL,
                'summary_score' => 78,
                'status' => 1,
                'created_at' => '2022-03-23 16:52:33',
                'updated_at' => '2022-03-23 16:53:45',
            ),
        ));
        
        
    }
}