<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;

use App\Http\Controllers\TeacherController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\ClassController;
use App\Http\Controllers\LessonController;
use App\Http\Controllers\ExamController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', [UserController::class, 'login'])->name('login');
Route::post('/register', [UserController::class, 'register'])->name('register');

Route::post('/list-user', [UserController::class, 'list_user'])->name('list_user');
Route::post('/delete-user', [UserController::class, 'delete_user'])->name('delete_user');
Route::post('/get-user', [UserController::class, 'get_user'])->name('get_user');
Route::post('/add-user', [UserController::class, 'add_user'])->name('add_user');
Route::post('/update-user', [UserController::class, 'update_user'])->name('update_user');

Route::post('/delete-teacher', [TeacherController::class, 'delete_teacher'])->name('delete_teacher');
Route::post('/get-teacher', [TeacherController::class, 'get_teacher'])->name('get_teacher');
Route::post('/list-teacher', [TeacherController::class, 'list_teacher'])->name('list_teacher');
Route::post('/add-teacher', [TeacherController::class, 'add_teacher'])->name('add_teacher');
Route::post('/update-teacher', [TeacherController::class, 'update_teacher'])->name('update_teacher');

Route::post('/get-teacher-lesson-options', [TeacherController::class, 'get_teacher_lesson_options'])->name('get_teacher_lesson_options');
Route::post('/list-teacher-lessons', [TeacherController::class, 'list_teacher_lessons'])->name('list_teacher_lessons');
Route::post('/add-teacher-lesson', [TeacherController::class, 'add_teacher_lesson'])->name('add_teacher_lesson');

Route::post('/delete-student', [StudentController::class, 'delete_student'])->name('delete_student');
Route::post('/get-student', [StudentController::class, 'get_student'])->name('get_student');
Route::post('/list-student', [StudentController::class, 'list_student'])->name('list_student');
Route::post('/add-student', [StudentController::class, 'add_student'])->name('add_student');
Route::post('/update-student', [StudentController::class, 'update_student'])->name('update_student');

Route::post('/delete-lesson', [LessonController::class, 'delete_lesson'])->name('delete_lesson');
Route::post('/get-lesson', [LessonController::class, 'get_lesson'])->name('get_lesson');
Route::post('/list-lesson', [LessonController::class, 'list_lesson'])->name('list_lesson');
Route::post('/add-lesson', [LessonController::class, 'add_lesson'])->name('add_lesson');
Route::post('/update-lesson', [LessonController::class, 'update_lesson'])->name('update_lesson');

Route::post('/delete-class', [ClassController::class, 'delete_class'])->name('delete_class');
Route::post('/get-class', [ClassController::class, 'get_class'])->name('get_class');
Route::post('/list-class', [ClassController::class, 'list_class'])->name('list_class');
Route::post('/add-class', [ClassController::class, 'add_class'])->name('add_class');
Route::post('/update-class', [ClassController::class, 'update_class'])->name('update_class');
Route::post('/list-class-students', [ClassController::class, 'list_class_students'])->name('list_class_students');

Route::post('/get-exam-result', [ExamController::class, 'get_exam_result'])->name('get_exam_result');
Route::post('/set-exam-result', [ExamController::class, 'set_exam_result'])->name('set_exam_result');
