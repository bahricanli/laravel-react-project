<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\Classes;
use App\Models\Students;
use App\Models\TeacherLessons;

class ClassController extends Controller
{
    public function list_class(Request $request) {

        $classes = Classes::where("status", 1)->get();

        return $this->output("json", ['status' => true, 'classes' => $classes]);
    }

    public function get_class(Request $request) {

        $validator = Validator::make($request->all(), [
            "id" => 'required'
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $id = $request->get("id");

        $class = Classes::where("status", 1)->where("id", $id)->first();

        return $this->output("json", ['status' => true, 'class' => $class]);
    }

    public function add_class(Request $request) {

        $validator = Validator::make($request->all(), [
            "className" => 'required',
            "year" => 'required',
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $class = new Classes();
        $class->class_name = $request->get("className");
        $class->year = $request->get("year");
        $class->status = 1;
        $class->save();

        return $this->output("json", ['status' => true]);

    }

    public function update_class(Request $request) {

        $validator = Validator::make($request->all(), [
            "id" => 'required',
            "className" => 'required',
            "year" => 'required',
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $id = $request->get("id");
        $class = Classes::where("id", $id)->first();
        if($class==null) {
            return $this->output("json", ['status' => false]);
        }
        else {
            $class->class_name = $request->get("className");
            $class->year = $request->get("year");
            $class->status = 1;
            $class->save();
            return $this->output("json", ['status' => true]);
        }

    }

    public function delete_class(Request $request) {

        $validator = Validator::make($request->all(), [
            "id" => 'required',
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $id = $request->get("id");
        $class = Classes::where("id", $id)->first();
        if($class==null) {
            return $this->output("json", ['status' => false]);
        }
        else {
            $class->status = 0;
            $class->save();
            return $this->output("json", ['status' => true]);
        }

    }

    public function list_class_students(Request $request) {

        $validator = Validator::make($request->all(), [
            "classId" => 'required',
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $classId = $request->get("classId");

        if($request->has("teacherLessonsId")) {
            $teacherLessonsId = $request->get("teacherLessonsId");
            $teacherLesson = TeacherLessons::where("id", $teacherLessonsId)->where("class_id", $classId)->first();
            $lesson_name = $teacherLesson->getLesson()->lesson_name;
            $lesson_id = $teacherLesson->lesson_id;
        }
        else {
            $lesson_name = "";
            $lesson_id = "";
        }

        $students = Students::where("status", 1)->where("class_id", $classId)->get();
        $class = Classes::where("id", $classId)->first();

        return $this->output("json", ['status' => true, 'class' => $class, 'students' => $students, 'lesson_name' => $lesson_name, 'lesson_id' => $lesson_id]);
    }

}
