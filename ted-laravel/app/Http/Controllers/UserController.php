<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Models\User;

class UserController extends Controller
{
    public function login(Request $request) {

        $validator = Validator::make($request->all(), [
            "email" => 'email|required',
            "password" => 'required',
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $email = $request->get("email");
        $password = $request->get("password");

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            return $this->output("json", ['status' => true, 'user' => Auth::user() ]);
        }
        else {
            return $this->output("json", ['status' => false, 'message' => "Kullanıcı bulunamadı"]);
        }
    }

    public function register(Request $request) {

        $validator = Validator::make($request->all(), [
            "name" => 'required',
            "email" => 'email|required',
            "password" => 'required',
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $email = $request->get("email");

        $user = User::where("email", $email)->first();
        if($user==null) {
            $user = new User();
            $user->name = $request->get("name");
            $user->email = $email;
            $user->password = Hash::make($request->get("password"));
            $user->save();

            return $this->output("json", ['status' => true, 'message' => "Kullanıcı başarı ile eklendi", 'user' => $user]);
        }
        else {
            return $this->output("json", ['status' => false, 'message' => "Kullanıcı mevcut"]);
        }

    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function list_user(Request $request) {

        $users = User::where("status", 1)->get();

        return $this->output("json", ['status' => true, 'users' => $users]);

    }

    public function get_user(Request $request) {

        $validator = Validator::make($request->all(), [
            "id" => 'required'
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $id = $request->get("id");

        $user = User::where("status", 1)->where("id", $id)->first();

        return $this->output("json", ['status' => true, 'user' => $user]);
    }

    public function add_user(Request $request) {

        $validator = Validator::make($request->all(), [
            "name" => 'required',
            "email" => 'email|required',
            "role" => 'required',
            "password" => 'required',
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $user = new User();
        $user->name = $request->get("name");
        $user->email = $request->get("email");
        $user->role = $request->get("role");
        $user->password = Hash::make($request->get("password"));
        $user->status = 1;
        $user->save();

        return $this->output("json", ['status' => true]);
    }

    public function update_user(Request $request) {

        $validator = Validator::make($request->all(), [
            "id" => 'required',
            "name" => 'required',
            "email" => 'required',
            "role" => 'required',
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $id = $request->get("id");
        $user = User::where("id", $id)->first();
        if($user==null) {
            return $this->output("json", ['status' => false]);
        }
        else {
            $user->name = $request->get("name");
            $user->email = $request->get("email");
            $user->role = $request->get("role");
            $user->status = 1;
            $user->save();
            return $this->output("json", ['status' => true]);
        }

    }

    public function delete_user(Request $request) {

        $validator = Validator::make($request->all(), [
            "id" => 'required',
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $id = $request->get("id");
        $user = User::where("id", $id)->first();
        if($user==null) {
            return $this->output("json", ['status' => false]);
        }
        else {
            $user->status = 0;
            $user->save();
            return $this->output("json", ['status' => true]);
        }

    }

}
