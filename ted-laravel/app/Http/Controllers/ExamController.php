<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\Students;
use App\Models\Teachers;
use App\Models\Classes;
use App\Models\Lessons;
use App\Models\TeacherLessons;
use App\Models\Exams;

class ExamController extends Controller
{
    public function get_exam_result(Request $request) {

        $validator = Validator::make($request->all(), [
            "studentId" => 'required',
            "classId" => 'required',
            "lessonId" => 'required',
            "teacherLessonsId" => 'required',
        ]);
        if ($validator->fails()) {
            $messages = "";
            foreach ($validator->errors()->All() as $message) {
                $messages .= $message." \n";
            }
            return $this->output("json", ['status' => false, 'message' => $messages]);
        }

        $studentId = $request->get("studentId");
        $classId = $request->get("classId");
        $lessonId = $request->get("lessonId");
        $teacherLessonsId = $request->get("teacherLessonsId");

        $teacherLesson = TeacherLessons::where("id", $teacherLessonsId)
                                        ->where("lesson_id", $lessonId)
                                        ->where("class_id", $classId)
                                        ->first();
        if($teacherLesson==null) {
            $teacherLesson = new TeacherLessons();
            $teacherLesson->lesson_id = $lessonId;
            $teacherLesson->class_id = $classId;
            $teacherLesson->status = 1;
            $teacherLesson->save();
        }

        $exam = Exams::where("teacher_lessons_id", $teacherLessonsId)->where("student_id", $studentId)->where("status", 1)->first();
        if($exam==null) $exam = new Exams();
        $class = Classes::where("id", $classId)->first();
        $lesson = Lessons::where("id", $lessonId)->first();
        $student = Students::where("id", $studentId)->first();

        return $this->output("json", [
            'status' => true,
            'exam' => $exam,
            'class' => $class,
            'lesson' => $lesson,
            'student' => $student,
        ]);
    }

/*
{
	"scores": {
		"exam_score_1": "50",
		"exam_score_2": "50"
	},
	"studentId": "1",
	"teacherLessonsId": "1",
	"summaryScore": 50
}
*/


    public function set_exam_result(Request $request) {

        $validator = Validator::make($request->all(), [
            "studentId" => 'required',
            "teacherLessonsId" => 'required',
            "scores" => 'required',
            "summaryScore" => 'required',
        ]);
        if ($validator->fails()) {
            $messages = "";
            foreach ($validator->errors()->All() as $message) {
                $messages .= $message." \n";
            }
            return $this->output("json", ['status' => false, 'message' => $messages]);
        }

        $studentId = $request->get("studentId");
        $teacherLessonsId = $request->get("teacherLessonsId");
        $scores = $request->get("scores");
        $summaryScore = $request->get("summaryScore");

        $exam = Exams::where("teacher_lessons_id", $teacherLessonsId)->where("student_id", $studentId)->where("status", 1)->first();
        if($exam==null) $exam = new Exams();
        $exam->teacher_lessons_id = $teacherLessonsId;
        $exam->student_id = $studentId;
        if(isset($scores["exam_score_1"])) $exam->exam_score_1 = $scores["exam_score_1"];
        if(isset($scores["exam_score_2"])) $exam->exam_score_2 = $scores["exam_score_2"];
        if(isset($scores["exam_score_3"])) $exam->exam_score_3 = $scores["exam_score_3"];
        if(isset($scores["performance_score_1"])) $exam->performance_score_1 = $scores["performance_score_1"];
        if(isset($scores["performance_score_2"])) $exam->performance_score_2 = $scores["performance_score_2"];
        if(isset($scores["project_score"])) $exam->project_score = $scores["project_score"];

        $sum = 0;
        $count = 0;
        foreach($scores as $key => $value) {

            if($value=="") continue;
            if( $value >= 0 ) {
                $sum = $sum + $value;
                $count++;
            }
        }

        $summaryScore = $sum / $count;
        $exam->summary_score = $summaryScore;
        $exam->status = 1;
        $exam->save();

        return $this->output("json", ['status' => true]);
    }
}
