<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\Lessons;

class LessonController extends Controller
{
    public function list_lesson(Request $request) {

        $lessons = Lessons::where("status", 1)->get();

        return $this->output("json", ['status' => true, 'lessons' => $lessons]);

    }

    public function get_lesson(Request $request) {

        $validator = Validator::make($request->all(), [
            "id" => 'required'
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $id = $request->get("id");

        $lesson = Lessons::where("status", 1)->where("id", $id)->first();

        return $this->output("json", ['status' => true, 'lesson' => $lesson]);
    }

    public function add_lesson(Request $request) {

        $validator = Validator::make($request->all(), [
            "lessonName" => 'required',
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $lesson = new Lessons();
        $lesson->lesson_name = $request->get("lessonName");
        $lesson->status = 1;
        $lesson->save();

        return $this->output("json", ['status' => true]);

    }

    public function update_lesson(Request $request) {

        $validator = Validator::make($request->all(), [
            "id" => 'required',
            "lessonName" => 'required',
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $id = $request->get("id");
        $lesson = Lessons::where("id", $id)->first();
        if($lesson==null) {
            return $this->output("json", ['status' => false]);
        }
        else {
            $lesson->lesson_name = $request->get("lessonName");
            $lesson->status = 1;
            $lesson->save();
            return $this->output("json", ['status' => true]);
        }

    }

    public function delete_lesson(Request $request) {

        $validator = Validator::make($request->all(), [
            "id" => 'required',
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $id = $request->get("id");
        $lesson = Lessons::where("id", $id)->first();
        if($lesson==null) {
            return $this->output("json", ['status' => false]);
        }
        else {
            $lesson->status = 0;
            $lesson->save();
            return $this->output("json", ['status' => true]);
        }

    }
}
