<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\Teachers;
use App\Models\Classes;
use App\Models\Lessons;
use App\Models\TeacherLessons;

class TeacherController extends Controller
{
    public function list_teacher(Request $request) {

        $teachers = Teachers::where("status", 1)->get();

        return $this->output("json", ['status' => true, 'teachers' => $teachers]);
    }

    public function get_teacher(Request $request) {

        $validator = Validator::make($request->all(), [
            "id" => 'required'
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $id = $request->get("id");

        $teacher = Teachers::where("status", 1)->where("id", $id)->first();

        return $this->output("json", ['status' => true, 'teacher' => $teacher]);
    }

    public function add_teacher(Request $request) {

        $validator = Validator::make($request->all(), [
            "teacherName" => 'required',
            "teacherSurname" => 'required'
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $teacher = new Teachers();
        $teacher->teacher_name = $request->get("teacherName");
        $teacher->teacher_surname = $request->get("teacherSurname");
        $teacher->status = 1;
        $teacher->save();

        return $this->output("json", ['status' => true]);
    }

    public function update_teacher(Request $request) {

        $validator = Validator::make($request->all(), [
            "id" => 'required',
            "teacherName" => 'required',
            "teacherSurname" => 'required'
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $id = $request->get("id");
        $teacher = Teachers::where("id", $id)->first();
        if($teacher==null) {
            return $this->output("json", ['status' => false]);
        }
        else {
            $teacher->teacher_name = $request->get("teacherName");
            $teacher->teacher_surname = $request->get("teacherSurname");
            $teacher->status = 1;
            $teacher->save();
            return $this->output("json", ['status' => true]);
        }

    }

    public function delete_teacher(Request $request) {

        $validator = Validator::make($request->all(), [
            "id" => 'required',
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $id = $request->get("id");
        $teacher = Teachers::where("id", $id)->first();
        if($teacher==null) {
            return $this->output("json", ['status' => false]);
        }
        else {
            $teacher->status = 0;
            $teacher->save();
            return $this->output("json", ['status' => true]);
        }

    }

    public function get_teacher_lesson_options(Request $request) {

        $classes = Classes::where("status", 1)->get();
        $lessons = Lessons::where("status", 1)->get();

        return $this->output("json", ['status' => true, 'classes' => $classes, 'lessons' => $lessons]);
    }

    public function list_teacher_lessons(Request $request) {

        $validator = Validator::make($request->all(), [
            "teacherId" => 'required',
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $teacherId = $request->get("teacherId");

        $newTecherLessons = [];
        $teacherLessons = TeacherLessons::where("teacher_id", $teacherId)->get();
        foreach($teacherLessons as $teacherLesson) {
            $newTecherLessons[] = [
                "id" => $teacherLesson->id,
                "class_id" => $teacherLesson->class_id,
                "class_name" => $teacherLesson->getClass()->class_name." (".$teacherLesson->getClass()->year.")",
                "lesson_name" => $teacherLesson->getLesson()->lesson_name
            ];
        }

        return $this->output("json", ['status' => true, 'teacherLessons' => $newTecherLessons]);
    }

    public function add_teacher_lesson(Request $request) {

        $validator = Validator::make($request->all(), [
            "teacherId" => 'required',
            "lessonId" => 'required',
            "classId" => 'required',
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $teacherId = $request->get("teacherId");
        $lessonId = $request->get("lessonId");
        $classId = $request->get("classId");

        $teacherLesson = TeacherLessons::where("teacher_id", $teacherId)
                                        ->where("lesson_id", $lessonId)
                                        ->where("class_id", $classId)
                                        ->first();
        if($teacherLesson==null) $teacherLesson= new TeacherLessons();
        $teacherLesson->teacher_id = $teacherId;
        $teacherLesson->lesson_id = $lessonId;
        $teacherLesson->class_id = $classId;
        $teacherLesson->status = 1;

        $teacherLesson->save();

        return $this->output("json", ['status' => true]);
    }

}
