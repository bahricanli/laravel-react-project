<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\Students;
use App\Models\Classes;

class StudentController extends Controller
{
    public function list_student(Request $request) {

        $newStudents = [];
        $students = Students::where("status", 1)->get();
        foreach($students as $student) {
            $class = Classes::where("id", $student->class_id)->first();
            if($class==null) $class = new Classes();
            $student->class_name = $class->class_name." (".$class->year.")";
            $newStudents[] = $student;
        }

        return $this->output("json", ['status' => true, 'students' => $students]);

    }

    public function get_student(Request $request) {

        $validator = Validator::make($request->all(), [
            "id" => 'required'
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $id = $request->get("id");

        $student = Students::where("status", 1)->where("id", $id)->first();
        $classes = Classes::where("status", 1)->get();

        return $this->output("json", ['status' => true, 'student' => $student, 'classes' => $classes]);
    }

    public function add_student(Request $request) {

        $validator = Validator::make($request->all(), [
            "studentName" => 'required',
            "studentSurname" => 'required',
            "identityNumber" => 'required',
            "classId" => 'required',
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $student = new Students();
        $student->identity_number = $request->get("identityNumber");
        $student->student_name = $request->get("studentName");
        $student->student_surname = $request->get("studentSurname");
        $student->class_id = $request->get("classId");
        $student->status = 1;
        $student->save();

        return $this->output("json", ['status' => true]);

    }

    public function update_student(Request $request) {

        $validator = Validator::make($request->all(), [
            "id" => 'required',
            "studentName" => 'required',
            "studentSurname" => 'required',
            "identityNumber" => 'required',
            "classId" => 'required',
        ]);
        if ($validator->fails()) {
            $messages = "";
            foreach ($validator->errors()->All() as $message) {
                $messages .= $message." \n";
            }
            return $this->output("json", ['status' => false, 'message' => $messages]);
        }

        $id = $request->get("id");
        $student = Students::where("id", $id)->first();
        if($student==null) {
            return $this->output("json", ['status' => false]);
        }
        else {
            $student->identity_number = $request->get("identityNumber");
            $student->student_name = $request->get("studentName");
            $student->student_surname = $request->get("studentSurname");
            $student->class_id = $request->get("classId");
            $student->status = 1;
            $student->save();
            return $this->output("json", ['status' => true]);
        }

    }

    public function delete_student(Request $request) {

        $validator = Validator::make($request->all(), [
            "id" => 'required',
        ]);
        	if ($validator->fails()) {
            	$messages = "";
		foreach ($validator->errors()->All() as $message) {
			$messages .= $message." \n";
		}
		$result = array('status' => false, 'message' => $messages);
		return $this->output("json", $result);
        }

        $id = $request->get("id");
        $student = Students::where("id", $id)->first();
        if($student==null) {
            return $this->output("json", ['status' => false]);
        }
        else {
            $student->status = 0;
            $student->save();
            return $this->output("json", ['status' => true]);
        }

    }
}
