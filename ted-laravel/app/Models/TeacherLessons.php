<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeacherLessons extends Model
{
    use HasFactory;

    public function getClass()
    {
        return $this->hasOne('App\Models\Classes', 'id', 'class_id')->first();
    }

    public function getLesson()
    {
        return $this->hasOne('App\Models\Lessons', 'id', 'lesson_id')->first();
    }
}
