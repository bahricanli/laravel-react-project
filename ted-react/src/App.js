import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import {BrowserRouter, Routes, Route} from 'react-router-dom'

import Home from './Home'
import User from './User'
import Class from './Class'
import Lesson from './Lesson'
import Student from './Student'
import Teacher from './Teacher'
import Exam from './Exam'

function App() {
  return (
    <div className="App">
      <BrowserRouter>

        <Routes>

          <Route path="/" element={<Home />} />
          <Route path="/home" element={<Home />} />

          <Route path="/login" element={<User.Login />} />
          <Route path="/register" element={<User.Register />} />
          <Route path="/logout" element={<User.Logout />} />

          <Route path="/list-user" element={<User.List />} />
          <Route path="/add-user" element={<User.Add />} />
          <Route path="/update-user/:id" element={<User.Update />} />

          <Route path="/list-class" element={<Class.List />} />
          <Route path="/add-class" element={<Class.Add />} />
          <Route path="/update-class/:id" element={<Class.Update />} />
          <Route path="/class-stutents/:class_id" element={<Class.Stutents />} />
          <Route path="/class-stutents/:class_id/:teacher_lessons_id" element={<Class.Stutents />} />
          

          <Route path="/list-lesson" element={<Lesson.List />} />
          <Route path="/add-lesson" element={<Lesson.Add />} />
          <Route path="/update-lesson/:id" element={<Lesson.Update />} />

          <Route path="/list-student" element={<Student.List />} />
          <Route path="/add-student" element={<Student.Add />} />
          <Route path="/update-student/:id" element={<Student.Update />} />          

          <Route path="/list-teacher" element={<Teacher.List />} />
          <Route path="/add-teacher" element={<Teacher.Add />} />
          <Route path="/update-teacher/:id" element={<Teacher.Update />} />
          <Route path="/teacher-lessons/:id" element={<Teacher.Lessons />} />
          <Route path="/add-teacher-lesson/:id" element={<Teacher.AddLesson />} />

          <Route path="/set-exam-result/:student_id/:class_id/:lesson_id/:teacher_lessons_id" element={<Exam.Set />} />
          

        </Routes>

      </BrowserRouter>
    </div>
  );
}

export default App;