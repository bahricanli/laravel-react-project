import React, {useState, useEffect} from 'react'
import {Table, Container, DropdownButton, Dropdown, Card, Form, Button, Col, Row} from 'react-bootstrap'
import {Link, useParams, useNavigate} from 'react-router-dom'

import Header from './Header'

async function postData(url = '', data = {}) {
  // Default options are marked with *
  const response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      'Content-Type': 'application/json'
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  });
  return response.json(); // parses JSON response into native JavaScript objects
}

const Teacher = {

    List() {

        const navigate = useNavigate();
        const [teacherList, setTeacherList] = useState([]);
        
        const DisplayData = teacherList.map( (item) => {
            return (
            <tr>
                <td>{item.id}</td>
                <td>{item.teacher_name}</td>
                <td>{item.teacher_surname}</td>
                <td>
                    <DropdownButton id="dropdown-basic-button" title="İşlemler">
                        <Link className="dropdown-item" to={"/update-teacher/"+item.id}>Düzenle</Link>         
                        <Dropdown.Item href="#" onClick={()=>deleteOperation(item.id)} >Sil</Dropdown.Item>     
                        <hr />
                        <Link className="dropdown-item" to={"/teacher-lessons/"+item.id}>Dersler</Link>       
                        <Link className="dropdown-item" to={"/add-teacher-lesson/"+item.id}>Ders Ekle</Link>                         
                    </DropdownButton>
                </td>
            </tr>
            )}
        )

        async function deleteOperation(id) {
            postData('https://api.tedproje.com/api/delete-teacher', {"id":id})
            .then(data => {
                if(data.status) {
                    navigate(0);
                }
                else {
                    alert(data.message);
                }
            });
        }
                
        useEffect( () => {
            postData('https://api.tedproje.com/api/list-teacher', {})
            .then(data => {
                if(data.status) {
                    setTeacherList( data.teachers );                    
                }
                else {
                    alert("Sorun oluştu");
                }                
            });
        }, []);

        return (
            <div>
                <Header />
                <Container>
                    <h1 style={{marginTop:'100px'}}>Öğretmenler</h1>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Öğretmen Adı</th>
                                <th>Öğretmen Soyadı</th>
                                <th>İşlemler</th>
                            </tr>
                        </thead>
                    <tbody>
                            {DisplayData}     
                    </tbody>
                    </Table>
                </Container>    
            </div>
        )
    },

    Add() {

        const navigate = useNavigate();
        const [teacherName, setTeacherName] = useState("");
        const [teacherSurname, setTeacherSurname] = useState("");

        const handleChange = (e) => {
            if(e.target.name === "teacher_name") {
                setTeacherName(e.target.value);
            }                
            else if(e.target.name === "teacher_surname") {
                setTeacherSurname(e.target.value);
            }                
        };

        const handleSubmit = (e) => {
            let item={teacherName, teacherSurname};
            postData('https://api.tedproje.com/api/add-teacher', item)
            .then(data => {
                if(data.status) {
                    navigate("/list-teacher");
                }
                else {
                    alert(data.message);
                }
            });
        };        


        return (
            <div>
                <Header/>
                <Container>                
                    <Card className="col-md-12" style={{ marginTop: '200px' }}>
                        <Card.Header style={{ width: '100%' }}>
                            <Button style={{float: 'left'}} onClick={() => navigate(-1)}>Geri</Button>
                            <Card.Title style={{display: 'inline-block'}}>Öğrenci Ekle</Card.Title>
                        </Card.Header>
                        <Card.Body style={{ width: '100%' }}>
                            <Form>   
                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Öğretmen Adı</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={teacherName} onChange={handleChange} name="teacher_name" type="text" placeholder="Öğretmen Adı" />                    
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Öğretmen Soyadı</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={teacherSurname} onChange={handleChange} name="teacher_surname" type="text" placeholder="Öğretmen Soyadı" />                    
                                    </Col>
                                </Form.Group>

                                <Button variant="primary" onClick={handleSubmit}>
                                    Ekle
                                </Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </Container>
            </div>
        )
    },

    Update() {

        const navigate = useNavigate();
        let params = useParams();
        let id = params.id;

        useEffect( () => {
            postData('https://api.tedproje.com/api/get-teacher', {"id":id})
            .then(data => {
                if(data.status) {
                    setTeacherName(data.teacher.teacher_name);                      
                    setTeacherSurname(data.teacher.teacher_surname);                      
                }
                else {
                    alert("Sorun oluştu");
                }                
            })
        }, []);

        const [teacherName, setTeacherName] = useState("");
        const [teacherSurname, setTeacherSurname] = useState("");

        const handleChange = (e) => {
            if(e.target.name === "teacher_name") {
                setTeacherName(e.target.value);
            }                
            else if(e.target.name === "teacher_surname") {
                setTeacherSurname(e.target.value);
            }                
        };

        const handleSubmit = (e) => {
            let item={id, teacherName, teacherSurname};
            postData('https://api.tedproje.com/api/update-teacher', item)
            .then(data => {
                if(data.status) {
                    navigate("/list-teacher");
                }
                else {
                    alert(data.message);
                }
            });
        };        

        return (
            <div>
                <Header/>
                <Container>                
                    <Card className="col-md-12" style={{ marginTop: '200px' }}>
                        <Card.Header style={{ width: '100%' }}>
                            <Button style={{float: 'left'}} onClick={() => navigate(-1)}>Geri</Button>
                            <Card.Title style={{display: 'inline-block'}}>Öğrenci Güncelle</Card.Title>
                        </Card.Header>
                        <Card.Body style={{ width: '100%' }}>
                            <Form>   
                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Öğretmen Adı</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={teacherName} onChange={handleChange} name="teacher_name" type="text" placeholder="Öğretmen Adı" />                    
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Öğretmen Soyadı</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={teacherSurname} onChange={handleChange} name="teacher_surname" type="text" placeholder="Öğretmen Soyadı" />                    
                                    </Col>
                                </Form.Group>

                                <Button variant="primary" onClick={handleSubmit}>
                                    Güncelle
                                </Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </Container>
            </div>
        )
    },

    Lessons() {

        const navigate = useNavigate();
        let params = useParams();
        let teacherId = params.id;

        const [teacherLessons, setTeacherLessons] = useState([]);
        
        const DisplayData = teacherLessons.map( (item) => {
            return (
            <tr>
                <td>{item.id}</td>
                <td>{item.class_name}</td>
                <td>{item.lesson_name}</td>
                <td>
                    <DropdownButton id="dropdown-basic-button" title="İşlemler">  
                        <Dropdown.Item href="#" onClick={()=>deleteOperation(item.id)} >Sil</Dropdown.Item> 
                        <hr />
                        <Link className="dropdown-item" to={"/class-stutents/"+item.class_id+"/"+item.id}>Öğrenciler</Link>
                    </DropdownButton>
                </td>
            </tr>
            )}
        )

        async function deleteOperation(id) {
            postData('https://api.tedproje.com/api/delete-teacher-lesson', {"id":id})
            .then(data => {
                if(data.status) {
                    navigate(0);
                }
                else {
                    alert(data.message);
                }
            });
        }
        
        useEffect( () => {
            postData('https://api.tedproje.com/api/list-teacher-lessons', {"teacherId": teacherId})
            .then(data => {
                if(data.status) {
                    setTeacherLessons( data.teacherLessons );                    
                }
                else {
                    alert("Sorun oluştu");
                }                
            });
        }, []);

        return (
            <div>
                <Header />
                <Container>
                    <h1 style={{marginTop:'100px'}}>Öğretmenin Dersleri</h1>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Sınıf</th>
                                <th>Ders</th>
                                <th>İşlemler</th>
                            </tr>
                        </thead>
                    <tbody>
                            {DisplayData}     
                    </tbody>
                    </Table>
                </Container>    
            </div>
        )
    },    


    AddLesson() {

        const navigate = useNavigate();
        let params = useParams();
        let teacherId = params.id;

        useEffect( () => {
            postData('https://api.tedproje.com/api/get-teacher-lesson-options', {"teacherId":teacherId})
            .then(data => {
                if(data.status) {
                    setLessons(data.lessons); 
                    setClasses(data.classes);                      
                }
                else {
                    alert("Sorun oluştu");
                }                
            })
        }, []);

        const [lessons, setLessons] = useState([]);
        const [classes, setClasses] = useState([]);

        const [lessonId, setLessonId] = useState("");
        const [classId, setClassId] = useState("");

        const handleChange = (e) => {
            if(e.target.name === "lesson_id") {                
                setLessonId(e.target.value);
            }
            else if(e.target.name === "class_id") {
                setClassId(e.target.value);
            }                
        };

        const handleSubmit = (e) => {
            let item={teacherId, lessonId, classId};
            postData('https://api.tedproje.com/api/add-teacher-lesson', item)
            .then(data => {
                if(data.status) {
                    navigate("/teacher-lessons/" + teacherId);
                }
                else {
                    alert(data.message);
                }
            });
        };        

        return (
            <div>
                <Header/>
                <Container>                
                    <Card className="col-md-12" style={{ marginTop: '200px' }}>
                        <Card.Header style={{ width: '100%' }}>
                            <Button style={{float: 'left'}} onClick={() => navigate(-1)}>Geri</Button>
                            <Card.Title style={{display: 'inline-block'}}>Ders Ekle</Card.Title>
                        </Card.Header>
                        <Card.Body style={{ width: '100%' }}>
                            <Form>   
                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Sınıf</Form.Label>
                                    <Col sm={9}>
                                        <Form.Select className="form-select" onChange={handleChange} name="class_id" aria-label="Sınıf seçin">
                                            <option value="">Sınıf seçiniz</option>
                                            {
                                                classes.map( (item) => {  
                                                    return (
                                                        <option value={item.id} >{item.class_name} ({item.year})</option>
                                                    )
                                                })
                                            }
                                        </Form.Select>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Ders</Form.Label>
                                    <Col sm={9}>
                                        <Form.Select className="form-select" onChange={handleChange} name="lesson_id" aria-label="Ders seçin">
                                            <option value="">Ders seçiniz</option>
                                             {
                                                lessons.map( (item) => {
                                                    return (
                                                        <option value={item.id} >{item.lesson_name}</option>
                                                    )
                                                })
                                            }
                                        </Form.Select>
                                    </Col>
                                </Form.Group>

                                <Button variant="primary" onClick={handleSubmit}>
                                    Ekle
                                </Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </Container>
            </div>
        )
    }

    
}

export default Teacher