import React, {useState, useEffect} from 'react'
import {Table, Container, DropdownButton, Dropdown, Card, Form, Button, Col, Row} from 'react-bootstrap'
import {Link, useParams, useNavigate} from 'react-router-dom'

import Header from './Header'

async function postData(url = '', data = {}) {
  // Default options are marked with *
  const response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      'Content-Type': 'application/json'
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  });
  return response.json(); // parses JSON response into native JavaScript objects
}

const Class = {

    List() {

        const navigate = useNavigate();
        const [classList, setClassList] = useState([]);
        
        const DisplayData = classList.map( (item) => {
            return (
            <tr>
                <td>{item.id}</td>
                <td>{item.class_name}</td>
                <td>{item.year}</td>
                <td>
                    <DropdownButton id="dropdown-basic-button" title="İşlemler">
                        <Link className="dropdown-item" to={"/update-class/"+item.id}>Düzenle</Link>         
                        <Dropdown.Item href="#" onClick={()=>deleteOperation(item.id)} >Sil</Dropdown.Item> 
                        <hr />
                        <Link className="dropdown-item" to={"/class-stutents/"+item.id}>Öğrenciler</Link>
                    </DropdownButton>
                </td>
            </tr>
            )}
        )

        async function deleteOperation(id) {
            postData('https://api.tedproje.com/api/delete-class', {"id":id})
            .then(data => {
                if(data.status) {
                    navigate(0);
                }
                else {
                    alert(data.message);
                }
            });
        }
        
        useEffect( () => {
            postData('https://api.tedproje.com/api/list-class', {})
            .then(data => {
                if(data.status) {
                    setClassList( data.classes );                    
                }
                else {
                    alert("Sorun oluştu");
                }                
            });
        }, []);

        return (
            <div>
                <Header />
                <Container>
                    <h1 style={{marginTop:'100px'}}>Sınıflar</h1>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Sınıf Adı</th>
                                <th>Eğitim Yılı</th>
                                <th>İşlemler</th>
                            </tr>
                        </thead>
                    <tbody>
                            {DisplayData}     
                    </tbody>
                    </Table>
                </Container>    
            </div>
        )
    },

    Add() {

        const navigate = useNavigate();
        const [className, setClassName] = useState("");
        const [year, setYear] = useState("");

        const handleChange = (e) => {
            if(e.target.name === "class_name") {
                setClassName(e.target.value);
            }  
            else if(e.target.name === "year") {
                setYear(e.target.value);
            }                
        };

        const handleSubmit = (e) => {
            let item={className, year};
            postData('https://api.tedproje.com/api/add-class', item)
            .then(data => {
                if(data.status) {
                    navigate("/list-class");
                }
                else {
                    alert(data.message);
                }
            });
        };

        return (
            <div>
                <Header/>
                <Container>                
                    <Card className="col-md-12" style={{ marginTop: '200px' }}>
                        <Card.Header style={{ width: '100%' }}>
                            <Button style={{float: 'left'}} onClick={() => navigate(-1)}>Geri</Button>
                            <Card.Title style={{display: 'inline-block'}}>Sınıf Ekle</Card.Title>
                        </Card.Header>
                        <Card.Body style={{ width: '100%' }}>
                            <Form>   
                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Sınıf Adı</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={className} onChange={handleChange} name="class_name" type="text" placeholder="Sınıf Giriniz" />                    
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Eğitim Yılı</Form.Label>
                                    <Col sm={9}>
                                        <Form.Select className="form-select" onChange={handleChange} name="year" aria-label="Eğitim Yılı">
                                            <option value="">Eğitim yılı seçiniz</option>
                                            <option value="2021 - 2022">2021 - 2022</option>
                                            <option value="2022 - 2023">2022 - 2023</option>
                                            <option value="2023 - 2024">2023 - 2024</option>
                                        </Form.Select>
                                    </Col>
                                </Form.Group>

                                <Button variant="primary" onClick={handleSubmit}>
                                    Ekle
                                </Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </Container>
            </div>
        )
    },

    Update() {

        const navigate = useNavigate();
        let params = useParams();
        let id = params.id;

        useEffect( () => {
            postData('https://api.tedproje.com/api/get-class', {"id":id})
            .then(data => {
                if(data.status) {
                    setClassName(data.class.class_name);   
                    setYear(data.class.year);                
                }
                else {
                    alert("Sorun oluştu");
                }                
            })
        }, []);

        const [className, setClassName] = useState("");
        const [year, setYear] = useState("");

        const handleChange = (e) => {
            if(e.target.name === "class_name") {
                setClassName(e.target.value);
            }        
            else if(e.target.name === "year") {
                setYear(e.target.value);
            }          
        };

        const handleSubmit = (e) => {
            let item={id, className, year};
            postData('https://api.tedproje.com/api/update-class', item)
            .then(data => {
                if(data.status) {
                    navigate("/list-class");
                }
                else {
                    alert(data.message);
                }
            });
        };

        return (
            <div>
                <Header/>
                <Container>                
                    <Card className="col-md-12" style={{ marginTop: '200px' }}>
                        <Card.Header style={{ width: '100%' }}>
                            <Button style={{float: 'left'}} onClick={() => navigate(-1)}>Geri</Button>
                            <Card.Title style={{display: 'inline-block'}}>Sınıf Güncelle</Card.Title>
                        </Card.Header>
                        <Card.Body style={{ width: '100%' }}>
                            <Form>   
                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Sınıf Adı</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={className} onChange={handleChange} name="class_name" type="text" placeholder="Sınıf Giriniz" />                    
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Eğitim Yılı</Form.Label>
                                    <Col sm={9}>
                                        <Form.Select className="form-select" onChange={handleChange} name="year" aria-label="Eğitim Yılı">
                                            <option value="">Eğitim yılı seçiniz</option>
                                            <option value="2021 - 2022" selected={year === '2021 - 2022'}>2021 - 2022</option>
                                            <option value="2022 - 2023" selected={year === '2022 - 2023'}>2022 - 2023</option>
                                            <option value="2023 - 2024" selected={year === '2023 - 2024'}>2023 - 2024</option>
                                        </Form.Select>
                                    </Col>
                                </Form.Group>

                                <Button variant="primary" onClick={handleSubmit}>
                                    Güncelle
                                </Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </Container>
            </div>
        )
    },

    Stutents() {

        let params = useParams();
        let classId = params.class_id;
        let teacherLessonsId = params.teacher_lessons_id;

        let parameters = [];

        if(teacherLessonsId != undefined) {
            parameters = {"classId":classId, "teacherLessonsId":teacherLessonsId};
        }
        else {
            parameters = {"classId":classId};
        }

        const [studentList, setStudentList] = useState([]);        
        const [className, setClassName] = useState("");
        const [lessonId, setLessonId] = useState("");
        const [lessonName, setLessonName] = useState("");
        

        
        
        const DisplayData = studentList.map( (item) => {
            return (
            <tr>
                <td>{item.id}</td>
                <td>{item.identity_number}</td>
                <td>{item.student_name}</td>
                <td>{item.student_surname}</td>
                <td>
                    {
                        lessonName?
                        <>                                                
                        <Link className="btn btn-primary" to={"/set-exam-result/"+item.id+"/"+classId+"/"+lessonId+"/"+teacherLessonsId}>Not Gir</Link>                                                         
                        </>
                        :
                        <>
                        -
                        </>
                    }                    
                </td>
            </tr>
            )}
        )
        
        useEffect( () => {
            postData('https://api.tedproje.com/api/list-class-students', parameters)
            .then(data => {
                if(data.status) {
                    setStudentList( data.students );      
                    setClassName( data.class.class_name );                       
                    if(data.lesson_name==="") {
                        setLessonName("");
                        setLessonId("");
                    }
                    else {
                        setLessonName( data.lesson_name + " dersini alan " );
                        setLessonId( data.lesson_id );
                    }
                }
                else {
                    setStudentList([]);              
                    alert("Sorun oluştu");
                }                
            });
        }, []);

        return (
            <div>
                <Header />
                <Container>
                    <h1 style={{marginTop:'100px'}}>{lessonName} {className} sınıfı öğrencileri</h1>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kimlik No</th>
                                <th>Öğrenci Adı</th>
                                <th>Öğrenci Soyadı</th>
                                <th>İşlemler</th>
                            </tr>
                        </thead>
                    <tbody>
                            {DisplayData}     
                    </tbody>
                    </Table>
                </Container>    
            </div>
        )
    },

}

export default Class
