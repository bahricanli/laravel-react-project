import React, {useState, useEffect} from 'react'
import {Form, Button, Col, Row, Card, Container, Table, DropdownButton, Dropdown } from 'react-bootstrap'
import { Link, useNavigate, useParams } from "react-router-dom";
import Header from './Header'

async function postData(url = '', data = {}) {
  // Default options are marked with *
  const response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      'Content-Type': 'application/json'
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  });
  return response.json(); // parses JSON response into native JavaScript objects
}

const User = {

    Login() {

        const [email, setEmail] = useState("");
        const [password, setPassword] = useState("");
        const navigate = useNavigate();

        // We need to spread the previous state and change the one we're targeting, so other data cannot be lost.
        const handleChange = (e) => {
            if(e.target.name === "email") {
                setEmail(e.target.value);
            }
            else if(e.target.name === "password") {
                setPassword(e.target.value);
            }        
        };

        const handleSubmit = (e) => {
            let item={email, password};
            postData('https://api.tedproje.com/api/login', item)
            .then(data => {
                if(data.status) {
                    localStorage.setItem("user-info", JSON.stringify(data.user));
                    navigate("/");
                }
                else {
                    alert(data.message);
                }
            });
        };

        return (        
            <div>
                <Header />
                <Container>
                    <Card className="col-md-6 offset-sm-3 align-items-center" style={{ marginTop: '200px' }}>
                        <Card.Header style={{ width: '100%' }}>
                            <Button style={{float: 'left'}} onClick={() => navigate(-1)}>Geri</Button>
                            <Card.Title style={{display: 'inline-block'}}>Giriş Yap</Card.Title>
                        </Card.Header>
                        <Card.Body style={{ width: '100%' }}>
                            <Form>              
                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Email</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={email} onChange={handleChange} name="email" type="email" placeholder="Email adresiniz" />                    
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Şifre</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={password} onChange={handleChange} name="password" type="password" placeholder="Şifreniz" />
                                    </Col>
                                </Form.Group>
                                
                                <Button variant="primary" onClick={handleSubmit}>
                                    Giriş Yap
                                </Button>

                            </Form>        
                        </Card.Body>
                    </Card>
                </Container>
            </div>
        )
    },

    Register() {

        const [name, setName] = useState("");
        const [email, setEmail] = useState("");
        const [password, setPassword] = useState("");
        const navigate = useNavigate();

        // We need to spread the previous state and change the one we're targeting, so other data cannot be lost.
        const handleChange = (e) => {
            if(e.target.name === "name") {
                setName(e.target.value);
            }
            else if(e.target.name === "email") {
                setEmail(e.target.value);
            }
            else if(e.target.name === "password") {
                setPassword(e.target.value);
            }        
        };

        const handleSubmit = (e) => {
            let item={name, email, password};
            postData('https://api.tedproje.com/api/register', item)
            .then(data => {
                console.log(data); // JSON data parsed by `data.json()` call
                if(data.status) {
                    localStorage.setItem("user-info", JSON.stringify(data.user));
                    navigate("/");
                }
                else {
                    alert(data.message);
                }
            });
        };

        return (
            <div>
                <Header />
                <Container style={{ marginTop: '200px' }}>
                    <Card className="col-md-6 offset-sm-3 align-items-center">
                        <Card.Header style={{ width: '100%' }}>
                            <Button style={{float: 'left'}} onClick={() => navigate(-1)}>Geri</Button>
                            <Card.Title style={{display: 'inline-block'}}>Üye Ol</Card.Title>
                        </Card.Header>
                        <Card.Body style={{ width: '100%' }}>
                            <Form>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Ad Soyad</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={name} onChange={handleChange} name="name" type="text" placeholder="Tam adınız" />                    
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Email</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={email} onChange={handleChange} name="email" type="email" placeholder="Email adresiniz" />                    
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Şifre</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={password} onChange={handleChange} name="password" type="password" placeholder="Şifreniz" />
                                    </Col>
                                </Form.Group>
                                
                                <Button variant="primary" onClick={handleSubmit}>
                                    Üye Ol
                                </Button>

                            </Form>        
                        </Card.Body>
                    </Card>
                </Container>
            </div>
        )
    },

    Logout() {

        const navigate = useNavigate();
        localStorage.removeItem("user-info");
        navigate("/login");

        return (
            <div>
                <Header />
                <h1 style={{marginTop:'100px'}}>Çıkış Yapıldı</h1>
            </div>
        )
    },

    List() {

        const navigate = useNavigate();
        const [userList, setUserList] = useState([]);
        
        const DisplayData = userList.map( (item) => {
            return (
            <tr>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.email}</td>
                <td>{item.role}</td>
                <td>
                    <DropdownButton id="dropdown-basic-button" title="İşlemler">
                        <Link className="dropdown-item" to={"/update-user/"+item.id}>Düzenle</Link>         
                        <Dropdown.Item href="#" onClick={()=>deleteOperation(item.id)} >Sil</Dropdown.Item>                              
                    </DropdownButton>
                </td>
            </tr>
            )}
        )

        async function deleteOperation(id) {
            postData('https://api.tedproje.com/api/delete-user', {"id":id})
            .then(data => {
                if(data.status) {
                    navigate(0);
                }
                else {
                    alert(data.message);
                }
            });
        }
                
        useEffect( () => {
            postData('https://api.tedproje.com/api/list-user', {})
            .then(data => {
                if(data.status) {
                    setUserList( data.users );                    
                }
                else {
                    alert("Sorun oluştu");
                }                
            });
        }, []);

        return (
            <div>
                <Header />
                <Container>
                    <h1 style={{marginTop:'100px'}}>Kullanıcılar</h1>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kullanıcı Tam Adı</th>
                                <th>Email</th>
                                <th>Görevi</th>
                                <th>İşlemler</th>
                            </tr>
                        </thead>
                    <tbody>
                            {DisplayData}     
                    </tbody>
                    </Table>
                </Container>    
            </div>
        )
    },

    Add() {

        const navigate = useNavigate();
        const [name, setName] = useState("");
        const [email, setEmail] = useState("");
        const [role, setRole] = useState("");
        const [password, setPassword] = useState("");

        const handleChange = (e) => {
            if(e.target.name === "name") {
                setName(e.target.value);
            }                
            else if(e.target.name === "email") {
                setEmail(e.target.value);
            }           
            else if(e.target.name === "role") {
                setRole(e.target.value);
            }                
            else if(e.target.name === "password") {
                setPassword(e.target.value);
            }                
        };

        const handleSubmit = (e) => {
            let item={name, email, role, password};
            postData('https://api.tedproje.com/api/add-user', item)
            .then(data => {
                if(data.status) {
                    navigate("/list-user");
                }
                else {
                    alert(data.message);
                }
            });
        };        


        return (
            <div>
                <Header/>
                <Container>                
                    <Card className="col-md-12" style={{ marginTop: '200px' }}>
                        <Card.Header style={{ width: '100%' }}>
                            <Button style={{float: 'left'}} onClick={() => navigate(-1)}>Geri</Button>
                            <Card.Title style={{display: 'inline-block'}}>Kullanıcı Ekle</Card.Title>
                        </Card.Header>
                        <Card.Body style={{ width: '100%' }}>
                            <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Kullanıcı Tam Adı</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={name} onChange={handleChange} name="name" type="text" placeholder="Kullanıcı Tam Adı" />                    
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Kullanıcı Email</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={email} onChange={handleChange} name="email" type="email" placeholder="Kullanıcı Email" />                    
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Kullanıcı Şifresi</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={password} onChange={handleChange} name="password" type="password" placeholder="Kullanıcı Şifresi" />                    
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Kullanıcı Rolü</Form.Label>
                                    <Col sm={9}>
                                        <Form.Select className="form-select" onChange={handleChange} name="role" aria-label="Kullanıcı Rolü">
                                            <option value="">Rol seçiniz</option>
                                            <option value="admin">Yönetici</option>
                                            <option value="teacher">Öğretmen</option>
                                        </Form.Select>
                                    </Col>
                                </Form.Group>

                                <Button variant="primary" onClick={handleSubmit}>
                                    Ekle
                                </Button>

                        </Card.Body>
                    </Card>
                </Container>
            </div>
        )
    },

    Update() {

        const navigate = useNavigate();
        let params = useParams();
        let id = params.id;

        useEffect( () => {
            postData('https://api.tedproje.com/api/get-user', {"id":id})
            .then(data => {
                if(data.status) {
                    setName(data.user.name);                      
                    setEmail(data.user.email);
                    setRole(data.user.role);                      
                }
                else {
                    alert("Sorun oluştu");
                }                
            })
        }, []);

        const [name, setName] = useState("");
        const [email, setEmail] = useState("");
        const [role, setRole] = useState("");

        const handleChange = (e) => {
            if(e.target.name === "name") {
                setName(e.target.value);
            }                
            else if(e.target.name === "email") {
                setEmail(e.target.value);
            }           
            else if(e.target.name === "role") {
                setRole(e.target.value);
            }                
        };

        const handleSubmit = (e) => {
            let item={id, name, email, role};
            postData('https://api.tedproje.com/api/update-user', item)
            .then(data => {
                if(data.status) {
                    navigate("/list-user");
                }
                else {
                    alert(data.message);
                }
            });
        };        

        return (
            <div>
                <Header/>
                <Container>                
                    <Card className="col-md-12" style={{ marginTop: '200px' }}>
                        <Card.Header style={{ width: '100%' }}>
                            <Button style={{float: 'left'}} onClick={() => navigate(-1)}>Geri</Button>
                            <Card.Title style={{display: 'inline-block'}}>Kullanıcı Güncelle</Card.Title>
                        </Card.Header>
                        <Card.Body style={{ width: '100%' }}>
                            <Form>   
                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Kullanıcı Tam Adı</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={name} onChange={handleChange} name="name" type="text" placeholder="Kullanıcı Tam Adı" />                    
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Kullanıcı Email</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={email} onChange={handleChange} name="email" type="text" placeholder="Kullanıcı Email" readOnly />                    
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Kullanıcı Rolü</Form.Label>
                                    <Col sm={9}>
                                        <Form.Select className="form-select" onChange={handleChange} name="role" aria-label="Kullanıcı Rolü">
                                            <option value="">Rol seçiniz</option>
                                            <option value="admin" selected={role === 'admin'}>Yönetici</option>
                                            <option value="teacher" selected={role === 'teacher'}>Öğretmen</option>
                                        </Form.Select>
                                    </Col>
                                </Form.Group>

                                <Button variant="primary" onClick={handleSubmit}>
                                    Güncelle
                                </Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </Container>
            </div>
        )
    }

}

export default User