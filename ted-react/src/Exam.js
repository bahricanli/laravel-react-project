import React, {useState, useEffect, Component} from 'react'
import {Table, Container, DropdownButton, Dropdown, Card, Form, Button, Col, Row} from 'react-bootstrap'
import {Link, useParams, useNavigate} from 'react-router-dom'
import { PencilSquare } from 'react-bootstrap-icons';

import Header from './Header'

async function postData(url = '', data = {}) {
  // Default options are marked with *
  const response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      'Content-Type': 'application/json'
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  });
  return response.json(); // parses JSON response into native JavaScript objects
}

const Exam = {

    Set() {

        const navigate = useNavigate();
        let params = useParams();
        let studentId = params.student_id;
        let classId = params.class_id;
        let lessonId = params.lesson_id;
        let teacherLessonsId = params.teacher_lessons_id;

        const [val_student, setStudent] = useState([]);
        const [val_lesson, setLesson] = useState([]);
        const [val_class, setClass] = useState([]);
        const [val_exam, setExam] = useState([]);

        const [scores, setScores] = useState({
            exam_score_1: "",
            exam_score_2: "",
            exam_score_3: "",
            performance_score_1: "",
            performance_score_2: "",
            project_score: "",
            summary_score: ""
        });

        const [exam_score_1, setExamScore1] = useState("");
        const [exam_score_2, setExamScore2] = useState("");
        const [exam_score_3, setExamScore3] = useState("");
        const [performance_score_1, setPerformanceScore1] = useState("");
        const [performance_score_2, setPerformanceScore2] = useState("");
        const [project_score, setProjectScore] = useState("");

        const [summary_score, setSummaryScore] = useState("");        

        const [readonly_exam_score_1, setExamScore1Readonly] = useState(true);
        const [readonly_exam_score_2, setExamScore2Readonly] = useState(true);
        const [readonly_exam_score_3, setExamScore3Readonly] = useState(true);
        const [readonly_performance_score_1, setPerformanceScore1Readonly] = useState(true);
        const [readonly_performance_score_2, setPerformanceScore2Readonly] = useState(true);
        const [readonly_project_score, setProjectScoreReadonly] = useState(true);
        
        useEffect( () => {
            postData('https://api.tedproje.com/api/get-exam-result', {
                "studentId":studentId, 
                "classId":classId, 
                "lessonId": lessonId, 
                "teacherLessonsId": teacherLessonsId
            })
            .then(data => {
                if(data.status) {
                    setStudent(data.student);
                    setLesson(data.lesson);
                    setClass(data.class);

                    setExamScore1( data.exam.exam_score_1 );     
                    setScoreHistoy("exam_score_1", data.exam.exam_score_1);
                    
                    setExamScore2( data.exam.exam_score_2 );
                    setScoreHistoy("exam_score_2", data.exam.exam_score_2);
                    
                    setExamScore3( data.exam.exam_score_3 );
                    setScoreHistoy("exam_score_3", data.exam.exam_score_3);
                    
                    setPerformanceScore1( data.exam.performance_score_1 );
                    setScoreHistoy("performance_score_1", data.exam.performance_score_1);
                    
                    setPerformanceScore2( data.exam.performance_score_2 );
                    setScoreHistoy("performance_score_2", data.exam.performance_score_2);

                    setProjectScore( data.exam.project_score );
                    setScoreHistoy("project_score", data.exam.project_score);

                    setSummaryScore( data.exam.summary_score );
                    setScoreHistoy("summary_score", data.exam.summary_score);

                }
                else {
                    alert("Sorun oluştu");
                }                
            });
        }, [])

        const historySummaryScore = (sum) => {
        
            if (typeof(summary_score) !== 'undefined' && summary_score != null) {
                document.getElementById('history_summary_score').innerHTML	= "Önce Not Ortalaması: " + summary_score;            
            }
                    
        }

        const setScoreHistoy = (newKey, newValue) => {

            if(newKey==="summary_score") return "";
            if (typeof(newValue) === 'undefined' || newValue === null) return "";
                                 
            setScores(prevState => {
                let scores = Object.assign({}, prevState.scores);  
                scores[newKey] = newValue;                     

                var total = 0;
                var examCount = 1;
                var sum = 0;
                Object.entries(scores).map( ([key,value]) => {
                    total = total + parseInt(value);
                    sum = total / examCount;
                    if(sum>=0) {
                        

                        setSummaryScore(sum);                        
                        historySummaryScore(sum);
                    }                    
                    examCount++;
                });

                return { scores };                             
            })
 
        }

        const handleChange = (e) => {
            
            if(e.target.name === "exam_score_1") {
                setExamScore1(e.target.value);                
            }    
            else if(e.target.name === "exam_score_2") {
                setExamScore2(e.target.value);
            }
            else if(e.target.name === "exam_score_3") {
                setExamScore3(e.target.value);
            }
            else if(e.target.name === "performance_score_1") {
                setPerformanceScore1(e.target.value);
            }
            else if(e.target.name === "performance_score_2") {
                setPerformanceScore2(e.target.value);
            }
            else if(e.target.name === "project_score") {
                setProjectScore(e.target.value);
            }                  
            else if(e.target.name === "summary_score") {
                setSummaryScore(e.target.value);
            }                  
        };   

        const handleSwitch = (e) => {            
            var selected_button = e.target.getAttribute("data-target");
            
            if(selected_button === "exam_score_1") {
                setExamScore1Readonly(!readonly_exam_score_1);
            }    
            else if(selected_button === "exam_score_2") {
                setExamScore2Readonly(!readonly_exam_score_2);
            }
            else if(selected_button === "exam_score_3") {
                setExamScore3Readonly(!readonly_exam_score_3);
            }
            else if(selected_button === "performance_score_1") {
                setPerformanceScore1Readonly(!readonly_performance_score_1);
            }
            else if(selected_button === "performance_score_2") {
                setPerformanceScore2Readonly(!readonly_performance_score_2);
            }
            else if(selected_button === "project_score") {
                setProjectScoreReadonly(!readonly_project_score);
            }          
          
        };  
       
        const handleBlur = (e) => {
            setScoreHistoy(e.target.name, e.target.value);
        }

        const handleSubmit = (e) => {
            let item=scores;
            item.studentId = studentId;
            item.teacherLessonsId = teacherLessonsId;
            item.summaryScore = summary_score;
            
            postData('https://api.tedproje.com/api/set-exam-result', item)
            .then(data => {
                if(data.status) {
                    navigate("/class-stutents/" + classId + "/" + teacherLessonsId);
                }
                else {
                    alert(data.message);
                }
            });
        };          

        return (
             <div>
                <Header/>
                <Container>                
                    <Card className="col-md-12" style={{ marginTop: '200px' }}>
                        <Card.Header style={{ width: '100%' }}>
                            <Button style={{float: 'left'}} onClick={() => navigate(-1)}>Geri</Button>
                            <Card.Title style={{display: 'inline-block'}}>{val_lesson.lesson_name}</Card.Title>
                        </Card.Header>
                        <Card.Body style={{ width: '100%' }}>
                            <Form>   
                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Öğrenci Adı</Form.Label>
                                    <Col sm={9}>
                                        <span className="form-control" style={{ textAlign: 'left' }}>{val_student.student_name} {val_student.student_surname}</span>
                                    </Col>
                                </Form.Group>
 	 	 	 	
                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>1. Yazılı Notu</Form.Label>
                                    <Col sm={4}>
                                        <Form.Control className="form-control" value={exam_score_1} onBlur={handleBlur} onChange={handleChange} name="exam_score_1" type="number" readOnly={readonly_exam_score_1}  />                    
                                    </Col>
                                    <Col sm={1}>
                                        <Button variant="secondary" onClick={handleSwitch} data-target="exam_score_1">
                                            D
                                        </Button>
                                    </Col>
                                    <Col sm={4}>
                                        <span id="history_exam_score_1"></span>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>2. Yazılı Notu</Form.Label>
                                    <Col sm={4}>
                                        <Form.Control className="form-control" value={exam_score_2} onBlur={handleBlur} onChange={handleChange} name="exam_score_2" type="number" readOnly={readonly_exam_score_2}  />                    
                                    </Col>
                                    <Col sm={1}>
                                        <Button variant="secondary" onClick={handleSwitch} data-target="exam_score_2">
                                            D
                                        </Button>
                                    </Col>
                                    <Col sm={4}>
                                        <span id="history_exam_score_2"></span>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>3. Yazılı Notu</Form.Label>
                                    <Col sm={4}>
                                        <Form.Control className="form-control" value={exam_score_3} onBlur={handleBlur} onChange={handleChange} name="exam_score_3" type="number" readOnly={readonly_exam_score_3}  />                    
                                    </Col>
                                    <Col sm={1}>
                                        <Button variant="secondary" onClick={handleSwitch} data-target="exam_score_3">
                                            D
                                        </Button>
                                    </Col>
                                    <Col sm={4}>
                                        <span id="history_exam_score_3"></span>
                                    </Col>
                                </Form.Group>


                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>1. Performans Notu</Form.Label>
                                    <Col sm={4}>
                                        <Form.Control className="form-control" value={performance_score_1} onBlur={handleBlur} onChange={handleChange} name="performance_score_1" type="number" readOnly={readonly_performance_score_1}  />                    
                                    </Col>
                                    <Col sm={1}>
                                        <Button variant="secondary" onClick={handleSwitch} data-target="performance_score_1">
                                            D
                                        </Button>
                                    </Col>
                                    <Col sm={4}>
                                        <span id="history_performance_score_1"></span>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>2. Performans Notu</Form.Label>
                                    <Col sm={4}>
                                        <Form.Control className="form-control" value={performance_score_2} onBlur={handleBlur} onChange={handleChange} name="performance_score_2" type="number" readOnly={readonly_performance_score_2}  />                    
                                    </Col>
                                    <Col sm={1}>
                                        <Button variant="secondary" onClick={handleSwitch} data-target="performance_score_2">
                                            D
                                        </Button>
                                    </Col>
                                    <Col sm={4}>
                                        <span id="history_performance_score_2"></span>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Proje Notu</Form.Label>
                                    <Col sm={4}>
                                        <Form.Control className="form-control" value={project_score} onBlur={handleBlur} onChange={handleChange} name="project_score" type="number"  readOnly={readonly_project_score} />                    
                                    </Col>
                                    <Col sm={1}>
                                        <Button variant="secondary" onClick={handleSwitch} data-target="project_score">
                                            D
                                        </Button>
                                    </Col>
                                    <Col sm={4}>
                                        <span id="history_project_score"></span>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Not Ortalaması</Form.Label>
                                    <Col sm={4}>
                                        <Form.Control id="summary_score" className="form-control" value={summary_score} onChange={handleChange} name="summary_score" type="number"  readOnly />                    
                                    </Col>
                                    <Col sm={5}>
                                        <span id="history_summary_score" className="form-control">&nbsp;</span>
                                    </Col>
                                </Form.Group>                    

                                <Button variant="primary" onClick={handleSubmit}>
                                    Kaydet
                                </Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </Container>
            </div>
        )

    }

}

export default Exam;

export const editIcon = () => {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
        </svg>
    )
}