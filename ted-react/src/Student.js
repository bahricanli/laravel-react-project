import React, {useState, useEffect} from 'react'
import {Table, Container, DropdownButton, Dropdown, Card, Form, Button, Col, Row} from 'react-bootstrap'
import {Link, useParams, useNavigate} from 'react-router-dom'

import Header from './Header'

async function postData(url = '', data = {}) {
  // Default options are marked with *
  const response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      'Content-Type': 'application/json'
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  });
  return response.json(); // parses JSON response into native JavaScript objects
}

const Student = {

List() {

        const navigate = useNavigate();
        const [studentList, setStudentList] = useState([]);
        
        const DisplayData = studentList.map( (item) => {
            return (
            <tr>
                <td>{item.id}</td>
                <td>{item.identity_number}</td>
                <td>{item.student_name}</td>
                <td>{item.student_surname}</td>
                <td>{item.class_name}</td>
                <td>
                    <DropdownButton id="dropdown-basic-button" title="İşlemler">
                        <Link className="dropdown-item" to={"/update-student/"+item.id}>Düzenle</Link>         
                        <Dropdown.Item href="#" onClick={()=>deleteOperation(item.id)} >Sil</Dropdown.Item>                              
                    </DropdownButton>
                </td>
            </tr>
            )}
        )

        async function deleteOperation(id) {
            postData('https://api.tedproje.com/api/delete-student', {"id":id})
            .then(data => {
                if(data.status) {
                    navigate(0);
                }
                else {
                    alert(data.message);
                }
            });
        }
        
        useEffect( () => {
            postData('https://api.tedproje.com/api/list-student', {})
            .then(data => {
                if(data.status) {
                    setStudentList( data.students );                    
                }
                else {
                    alert("Sorun oluştu");
                }                
            });
        }, []);

        return (
            <div>
                <Header />
                <Container>
                    <h1 style={{marginTop:'100px'}}>Öğrenciler</h1>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kimlik No</th>
                                <th>Öğrenci Adı</th>
                                <th>Öğrenci Soyadı</th>
                                <th>Sınıf</th>
                                <th>İşlemler</th>
                            </tr>
                        </thead>
                    <tbody>
                            {DisplayData}     
                    </tbody>
                    </Table>
                </Container>    
            </div>
        )
    },

    Add() {

        const navigate = useNavigate();
        const [studentName, setStudentName] = useState("");
        const [studentSurname, setStudentSurname] = useState("");
        const [identityNumber, setIdentityNumber] = useState("");

        const handleChange = (e) => {
            if(e.target.name === "student_name") {
                setStudentName(e.target.value);
            }                
            else if(e.target.name === "student_surname") {
                setStudentSurname(e.target.value);
            }    
            else if(e.target.name === "identity_number") {
                setIdentityNumber(e.target.value);
            }             
        };

        const handleSubmit = (e) => {
            let item={studentName, studentSurname, identityNumber};
            postData('https://api.tedproje.com/api/add-student', item)
            .then(data => {
                if(data.status) {
                    navigate("/list-student");
                }
                else {
                    alert(data.message);
                }
            });
        };  

        return (
            <div>
                <Header/>
                <Container>                
                    <Card className="col-md-12" style={{ marginTop: '200px' }}>
                        <Card.Header style={{ width: '100%' }}>
                            <Button style={{float: 'left'}} onClick={() => navigate(-1)}>Geri</Button>
                            <Card.Title style={{display: 'inline-block'}}>Öğrenci Ekle</Card.Title>
                        </Card.Header>
                        <Card.Body style={{ width: '100%' }}>
                            <Form>   

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Kimlik Numarası</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={identityNumber} onChange={handleChange} name="identity_number" type="text" placeholder="Kimlik Numarası" />                    
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Öğrenci Adı</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={studentName} onChange={handleChange} name="student_name" type="text" placeholder="Öğrenci Adı" />                    
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Öğrenci Soyadı</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={studentSurname} onChange={handleChange} name="student_surname" type="text" placeholder="Öğrenci Soyadı" />                    
                                    </Col>
                                </Form.Group>

                                <Button variant="primary" onClick={handleSubmit}>
                                    Ekle
                                </Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </Container>
            </div>
        )
    },

    Update() {

        const navigate = useNavigate();
        let params = useParams();
        let id = params.id;

        useEffect( () => {
            postData('https://api.tedproje.com/api/get-student', {"id":id})
            .then(data => {
                if(data.status) {
                    setStudentName(data.student.student_name);                      
                    setStudentSurname(data.student.student_surname);    
                    setIdentityNumber(data.student.identity_number);                      
                }
                else {
                    alert("Sorun oluştu");
                }                
            })
        }, []);

        const [studentName, setStudentName] = useState("");
        const [studentSurname, setStudentSurname] = useState("");
        const [identityNumber, setIdentityNumber] = useState("");
        

        const handleChange = (e) => {
            if(e.target.name === "student_name") {
                setStudentName(e.target.value);
            }                
            else if(e.target.name === "student_surname") {
                setStudentSurname(e.target.value);
            }   
            else if(e.target.name === "identity_number") {
                setIdentityNumber(e.target.value);
            }               
        };

        const handleSubmit = (e) => {
            let item={id, studentName, studentSurname, identityNumber};
            postData('https://api.tedproje.com/api/update-student', item)
            .then(data => {
                if(data.status) {
                    navigate("/list-student");
                }
                else {
                    alert(data.message);
                }
            });
        };        

        return (
            <div>
                <Header/>
                <Container>                
                    <Card className="col-md-12" style={{ marginTop: '200px' }}>
                        <Card.Header style={{ width: '100%' }}>
                            <Button style={{float: 'left'}} onClick={() => navigate(-1)}>Geri</Button>
                            <Card.Title style={{display: 'inline-block'}}>Öğrenci Güncelle</Card.Title>
                        </Card.Header>
                        <Card.Body style={{ width: '100%' }}>
                            <Form>  

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Kimlik Numarası</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={identityNumber} onChange={handleChange} name="identity_number" type="text" placeholder="Kimlik Numarası" />                    
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Öğrenci Adı</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={studentName} onChange={handleChange} name="student_name" type="text" placeholder="Öğrenci Adı" />                    
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Öğrenci Soyadı</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={studentSurname} onChange={handleChange} name="student_surname" type="text" placeholder="Öğrenci Soyadı" />                    
                                    </Col>
                                </Form.Group>

                                <Button variant="primary" onClick={handleSubmit}>
                                    Güncelle
                                </Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </Container>
            </div>
        )
    }

}

export default Student