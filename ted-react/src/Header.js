import {Navbar, Nav, NavDropdown, Container} from 'react-bootstrap'
import {NavLink} from 'react-router-dom'

function Header() {

    let activeStyle = {
        fontWeight: "bold"
    };

    let userInfo = localStorage.getItem("user-info")?localStorage.getItem("user-info"):null;

    let isAdmin = false;
    let isTeacher = false;

    if(userInfo!=null) {
        userInfo = JSON.parse(userInfo);

        if(userInfo.role==='admin') {
            isAdmin = true;
        }        
        else if(userInfo.role==='teacher') {
            isTeacher = true;
        }        
    }

    return (
        <div>
            <Navbar bg="light" expand="lg">
                <Container>
                    <Navbar.Brand href="/">Ted Project</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        {                    
                            ( (isAdmin) && (userInfo) )?
                            <>
                                <NavDropdown title="Kullanıcılar" id="basic-nav-dropdown">
                                    <NavLink
                                        className="dropdown-item"
                                        to="/list-user"
                                        style={({ isActive }) =>
                                        isActive ? activeStyle : undefined
                                        }
                                    >
                                        Kullanıcılar
                                    </NavLink>

                                    <NavLink
                                        className="dropdown-item"
                                        to="/add-user"
                                        style={({ isActive }) =>
                                        isActive ? activeStyle : undefined
                                        }
                                    >
                                        Kullanıcı Ekle
                                    </NavLink>                                
                                </NavDropdown>

                                <NavDropdown title="Öğretmenler" id="basic-nav-dropdown">
                                    <NavLink
                                        className="dropdown-item"
                                        to="/list-teacher"
                                        style={({ isActive }) =>
                                        isActive ? activeStyle : undefined
                                        }
                                    >
                                        Öğretmenler
                                    </NavLink>

                                    <NavLink
                                        className="dropdown-item"
                                        to="/add-teacher"
                                        style={({ isActive }) =>
                                        isActive ? activeStyle : undefined
                                        }
                                    >
                                        Öğretmen Ekle
                                    </NavLink>                                
                                </NavDropdown>

                                <NavDropdown title="Öğrenciler" id="basic-nav-dropdown">
                                    <NavLink
                                        className="dropdown-item"
                                        to="/list-student"
                                        style={({ isActive }) =>
                                        isActive ? activeStyle : undefined
                                        }
                                    >
                                        Öğrenciler
                                    </NavLink>                            

                                    <NavLink
                                        className="dropdown-item"
                                        to="/add-student"
                                        style={({ isActive }) =>
                                        isActive ? activeStyle : undefined
                                        }
                                    >
                                        Öğrenci Ekle
                                    </NavLink>

                                    
                                </NavDropdown>

                                <NavDropdown title="Sınıflar" id="basic-nav-dropdown">
                                    <NavLink
                                        className="dropdown-item"
                                        to="/list-class"
                                        style={({ isActive }) =>
                                        isActive ? activeStyle : undefined
                                        }
                                    >
                                        Sınıflar
                                    </NavLink>

                                    <NavLink
                                        className="dropdown-item"
                                        to="/add-class"
                                        style={({ isActive }) =>
                                        isActive ? activeStyle : undefined
                                        }
                                    >
                                        Sınıf Ekle
                                    </NavLink>

                                            
                                </NavDropdown>

                                <NavDropdown title="Dersler" id="basic-nav-dropdown">
                                    <NavLink
                                        className="dropdown-item"
                                        to="/list-lesson"
                                        style={({ isActive }) =>
                                        isActive ? activeStyle : undefined
                                        }
                                    >
                                        Dersler
                                    </NavLink>  
                                    <NavLink
                                        className="dropdown-item"
                                        to="/add-lesson"
                                        style={({ isActive }) =>
                                        isActive ? activeStyle : undefined
                                        }
                                    >
                                        Ders Ekle
                                    </NavLink>                                                            
                                </NavDropdown>
                            </>
                            :
                            <> 
                            </>
                            }
                            {
                            ( (isTeacher) && (userInfo) )?
                            <>
                            <NavLink
                                    className="nav-link"
                                    to="/list-class"
                                    style={({ isActive }) =>
                                    isActive ? activeStyle : undefined
                                    }
                                >
                                    Sınıflar
                            </NavLink>
                            </>
                            :
                            <>
                            </>
                            }
                            {                                                        

                            userInfo?
                            <>

                            <NavLink
                                    className="nav-link"
                                    to="/logout"
                                    style={({ isActive }) =>
                                    isActive ? activeStyle : undefined
                                    }
                                >
                                    Çıkış Yap
                            </NavLink>

                            </>
                            :
                            <>

                            <NavLink
                                className="nav-link"
                                to="/login"
                                style={({ isActive }) =>
                                isActive ? activeStyle : undefined
                                }
                            >
                                Giriş Yap
                            </NavLink>

                            <NavLink
                                className="nav-link"
                                to="/register"
                                style={({ isActive }) =>
                                isActive ? activeStyle : undefined
                                }
                            >
                                Üye Ol
                            </NavLink>                                                              
                            
                            </>
                        }

                        




                    </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
    )
}

export default Header