import React, {useState, useEffect} from 'react'
import {Table, Container, DropdownButton, Dropdown, Card, Form, Button, Col, Row} from 'react-bootstrap'
import {Link, useParams, useNavigate} from 'react-router-dom'

import Header from './Header'

async function postData(url = '', data = {}) {
  // Default options are marked with *
  const response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      'Content-Type': 'application/json'
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  });
  return response.json(); // parses JSON response into native JavaScript objects
}

const Lesson = {

    List() {

        const navigate = useNavigate();
        const [lessonList, setLessonList] = useState([]);
        
        const DisplayData = lessonList.map( (item) => {
            return (
            <tr>
                <td>{item.id}</td>
                <td>{item.lesson_name}</td>
                <td>
                    <DropdownButton id="dropdown-basic-button" title="İşlemler">
                        <Link className="dropdown-item" to={"/update-lesson/"+item.id}>Düzenle</Link>         
                        <Dropdown.Item href="#" onClick={()=>deleteOperation(item.id)} >Sil</Dropdown.Item>                              
                    </DropdownButton>
                </td>
            </tr>
            )}
        )

        async function deleteOperation(id) {
            postData('https://api.tedproje.com/api/delete-lesson', {"id":id})
            .then(data => {
                if(data.status) {
                    navigate(0);
                }
                else {
                    alert(data.message);
                }
            });
        }
                
        useEffect( () => {
            postData('https://api.tedproje.com/api/list-lesson', {})
            .then(data => {
                if(data.status) {
                    setLessonList( data.lessons );                    
                }
                else {
                    alert("Sorun oluştu");
                }                
            });
        }, []);

        return (
            <div>
                <Header />
                <Container>
                    <h1 style={{marginTop:'100px'}}>Dersler</h1>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Ders Adı</th>
                                <th>İşlemler</th>
                            </tr>
                        </thead>
                    <tbody>
                            {DisplayData}     
                    </tbody>
                    </Table>
                </Container>    
            </div>
        )
    },

    Add() {
        
        const navigate = useNavigate();
        const [lessonName, setLessonName] = useState("");

        const handleChange = (e) => {
            if(e.target.name === "lesson_name") {
                setLessonName(e.target.value);
            }                
        };

        const handleSubmit = (e) => {
            let item={lessonName};
            postData('https://api.tedproje.com/api/add-lesson', item)
            .then(data => {
                if(data.status) {
                    navigate("/list-lesson");
                }
                else {
                    alert(data.message);
                }
            });
        };    

        return (
            <div>
                <Header/>
                <Container>                
                    <Card lessonName="col-md-12" style={{ marginTop: '200px' }}>
                        <Card.Header style={{ width: '100%' }}>
                            <Button style={{float: 'left'}} onClick={() => navigate(-1)}>Geri</Button>
                            <Card.Title style={{display: 'inline-block'}}>Ders Ekle</Card.Title>
                        </Card.Header>
                        <Card.Body style={{ width: '100%' }}>
                            <Form>   
                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Ders Adı</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={lessonName} onChange={handleChange} name="lesson_name" type="text" placeholder="Sınıf Giriniz" />                    
                                    </Col>
                                </Form.Group>

                                <Button variant="primary" onClick={handleSubmit}>
                                    Ekle
                                </Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </Container>
            </div>
        )
    },

    Update() {

        const navigate = useNavigate();
        let params = useParams();
        let id = params.id;

        useEffect( () => {
            postData('https://api.tedproje.com/api/get-lesson', {"id":id})
            .then(data => {
                if(data.status) {
                    setLessonName(data.lesson.lesson_name);                      
                }
                else {
                    alert("Sorun oluştu");
                }                
            })
        }, []);

        const [lessonName, setLessonName] = useState("");

        const handleChange = (e) => {
            if(e.target.name === "lesson_name") {
                setLessonName(e.target.value);
            }                
        };

        const handleSubmit = (e) => {
            let item={id, lessonName};
            postData('https://api.tedproje.com/api/update-lesson', item)
            .then(data => {
                if(data.status) {
                    navigate("/list-lesson");
                }
                else {
                    alert(data.message);
                }
            });
        };        

        return (
            <div>
                <Header/>
                <Container>                
                    <Card lessonName="col-md-12" style={{ marginTop: '200px' }}>
                        <Card.Header style={{ width: '100%' }}>
                            <Button style={{float: 'left'}} onClick={() => navigate(-1)}>Geri</Button>
                            <Card.Title style={{display: 'inline-block'}}>Ders Güncelle</Card.Title>
                        </Card.Header>
                        <Card.Body style={{ width: '100%' }}>
                            <Form>   
                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm={3}>Ders Adı</Form.Label>
                                    <Col sm={9}>
                                        <Form.Control className="form-control" value={lessonName} onChange={handleChange} name="lesson_name" type="text" placeholder="Sınıf Giriniz" />                    
                                    </Col>
                                </Form.Group>

                                <Button variant="primary" onClick={handleSubmit}>
                                    Güncelle
                                </Button>
                            </Form>
                        </Card.Body>
                    </Card>
                </Container>
            </div>
        )
    }

}

export default Lesson